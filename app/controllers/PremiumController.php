<?php
use Omnipay\Omnipay;
use Carbon\Carbon;
class PremiumController extends BaseController
{
    public function index()
    {
        return View::make('premium/index');
    }
    
    public function pay()
    {
        $input = Input::all();
        
        if (isset($input['server_id'])) {
            $server_id = $input['server_id'];
        } elseif (Auth::check()) {
            $server_id = Auth::user()->id;
        } else {
            Session::flash('danger', Lang::get('server.messages.premium.logged'));
            return Redirect::guest('login');
        }
        
        if (($input['amount'] > 50) || ($input['amount'] < 1) || (($input['amount'] % 1) !== 0)) {
            Session::flash('danger', Lang::get('server.messages.premium.invalidamount'));
            return Redirect::route('premium');
        } else {
            $amount = number_format($input['amount'], 2);
        }
        
        $gateway = Omnipay::create('PayPal_Express');
        $gateway->setUsername('betaling_api1.bedrijfswebsitegelderland.nl');
        $gateway->setPassword('4PXBU7NL83C6GSV3');
        $gateway->setSignature('AoRJ5l7ToboxSXAIjs3AV.Th8jE8AMBcgc..-t9Bp-Yo3u7uCL8LTUs2');
        // $gateway->setTestMode(true);
        
        $options  = array(
            'description' => 'Premium at ' . SERVERLIST_NAME,
            'transactionId' => $server_id . Carbon::now()->timestamp,
            'cancelUrl' => url('/'),
            'returnUrl' => route('premium.success') . '?server_id=' . $server_id,
            'amount' => $amount,
            'currency' => 'EUR'
        );
        $response = $gateway->purchase($options)->send();
        $ref      = $response->getTransactionReference();
        Cache::put('order_object_' . $server_id . '_' . $ref, json_encode($options), 30);
        return Redirect::to($response->getRedirectUrl());
    }
    
    public function success()
    {
        $input     = Input::all();
        $server_id = $input['server_id'];
        $gateway   = Omnipay::create('PayPal_Express');
        $gateway->setUsername('betaling_api1.bedrijfswebsitegelderland.nl');
        $gateway->setPassword('4PXBU7NL83C6GSV3');
        $gateway->setSignature('AoRJ5l7ToboxSXAIjs3AV.Th8jE8AMBcgc..-t9Bp-Yo3u7uCL8LTUs2');
        // $gateway->setTestMode(true);
        
        $ref      = $input['token'];
        $options  = json_decode(Cache::get('order_object_' . $server_id . '_' . $ref), true);
        $response = $gateway->completePurchase($options)->send();
        $data     = $response->getData();
        
        $isAck  = isset($data['ACK']) && in_array($data['ACK'], array(
            'Success',
            'SuccessWithWarning'
        ));
        $isPaid = true;
        if (isset($data['PAYMENTSTATUS']) && in_array(strtolower($data['PAYMENTSTATUS']), array(
            'denied',
            'failed',
            'expired',
            'voided'
        ))) {
            $isPaid = false;
        }
        if ($isAck && $isPaid /*&& !(isset($data['L_SHORTMESSAGE0']) && strtolower($data['L_SHORTMESSAGE0']) === 'duplicate request')*/ ) {
            // Success
            $amount = $data['PAYMENTINFO_0_AMT'];
            
            // Upgrade Server
            $server  = Server::find($server_id);
            $premium = $server->premium();
            if (!empty($premium->get()->toArray())) {
                $premium->delete();
            }
            $server->addPremium($amount);
 
				Mail::send('mail/premium/new', array(
                	'name' => $server->name,
					'amount' => $amount
					), function($message) use ($server)
					{
						$message->to($server->email)->subject('Premium');
					});
            
            Session::flash('success', Lang::get('server.messages.premium.success'));
            return Redirect::route('server.view', array(
                $server->name
            ));
        } else {
            die('Invalid payment.');
        }
    }
}