<?php
use Carbon\Carbon;
class ApiController extends BaseController
{
    public function index($name)
    {
        $server = Server::withCache($name);
        
        $recent = array();
        foreach ($server->votes()->where('player', '!=', '')->orderBy('id', 'desc')->get() as $vote) {
            array_push($recent, $vote->player);
        }
        
        $times = array();
        foreach ($server->votes()->where('player', '!=', '')->orderBy('id', 'desc')->get() as $vote) {
            if (isset($times[$vote->player]))
                $times[$vote->player] = $times[$vote->player] + 1;
            $times[$vote->player] = 1;
        }
        
        $data = array(
            'name' => $server->name,
            'ip' => $server->ip,
            'port' => $server->port,
            'status' => ((bool) @$server->cache->status) ? 'online' : 'offline',
            'uptime' => $server->uptime,
            'poll' => $server->cache->updated_at->timestamp,
            'motd' => $server->cache->motd,
            'version' => $server->cache->version,
            'score' => $server->score,
            'players' => array(
                'count' => $server->cache->players->first()->count,
                'max' => $server->cache->players->first()->max,
                'names' => json_decode($server->cache->players->first()->names)
            ),
            'votes' => array(
                'count' => $server->votes()->count(),
                'players' => array(
                    'recent' => $recent,
                    'times' => $times
                )
            )
        );
        return Response::json($data);
    }
    
    public function players($name)
    {
        $server = Server::nameOrFail($name);
        
        $players = $server->cache()->get()->first()->players()->orderBy('id', 'desc')->take(7500)->remember(5, 'player_history_cache_' . strtolower($name))->get(array(
            'created_at',
            'count'
        ))->toArray();
        return Response::json(array_reverse(array_map(function($a)
        {
            return array(
                intval((string) strtotime($a['created_at']) . "000"),
                (int) $a['count']
            );
        }, $players)));
        
    }
    
    public function playersjs($name)
    {
        $contents = 'document.write(\'<iframe src="' . URL::route('api.chart', array(
            $name,
            200
        )) . '" frameborder="no" scrolling="no" width="100%" height="220px"></iframe>\')';
        $response = Response::make($contents, 200);
        $response->header('Content-Type', 'application/javascript');
        return $response;
    }
    
    public function playerlist($name)
    {
        $server = Server::withCache($name);
        
        // Generate content
        $content = '';
        foreach (json_decode($server->cache->lastPlayers->first()->names) as $player) {
            $content .= '<span title="' . $player . '"><img src="' . Avatar::url($player, 32) . '"></img></span>';
        }
        
        $style = '.playerlist img { border-radius: 2px 2px 2px 2px; padding-bottom: 4px; padding-right: 4px;} .playerlist span { position: relative; cursor: pointer; }  .playerlist span:hover:after { content: attr(title); position: absolute; white-space: nowrap; background: rgba(0, 0, 0, 0.85); padding: 3px 7px; color: #FFF; border-radius: 3px; -moz-border-radius: 3px; -webkit-border-radius: 3px; margin-left: -37px; margin-top: -25px; z-index: 99999;}';
        
        // Display our javascript
        $contents = View::make('api/playerlist', array(
            'content' => $content,
            'style' => $style
        ));
        $response = Response::make($contents, 200);
        $response->header('Content-Type', 'application/javascript');
        return $response;
    }
    
    public function badge($name)
    {
        $server = Server::nameOrFail($name);
        
        if ($server->cache->exists && $server->cache->status) {
            $image = public_path() . '/' . 'status' . '/online.png';
        } else {
            $image = public_path() . '/' . 'status' . '/offline.png';
        }
        $contents = file_get_contents($image);
        $response = Response::make($contents, 200);
        $response->header('Content-Type', 'image/png');
        return $response;
    }
    
    public function button($name)
    {
        $server = Server::nameOrFail($name);
        
        return View::make('api/button', array(
            'server' => $server
        ));
    }
    
    public function buttonjs($name)
    {
        $contents = 'document.write(\'<iframe src="' . URL::route('api.button', array(
            $name
        )) . '" frameborder="0" scrolling="no" height="45px" width="240px" allowtransparency="true" marginheight="0" marginwidth="0"></iframe>\')';
        $response = Response::make($contents, 200);
        $response->header('Content-Type', 'application/javascript');
        return $response;
    }
    
    public function premium($amount) 
    {
        $premiums = Premium::getPremiums($amount, true);
        $response = Array();
        
        foreach ($premiums as $premium) {
            $entry = array(
                'name' => $premium->server->name,
                'url' => URL::route('server.view', array($premium->server->name)),
                'banner' => (!empty($premium->server->banner)) ? URL::to($premium->server->banner) : URL::route('banner.small', $premium->server->name)
            );
            array_push($response, $entry);
        }
        
        return Response::make(json_encode(
            $response, JSON_UNESCAPED_SLASHES
        ))->header('Content-Type', "application/json");
    }
}