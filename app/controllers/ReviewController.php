<?php
class ReviewController extends BaseController
{
    public function post($name)
    {
        $server = Server::nameOrFail($name);
        
        $wrong = false;
        if (Input::get('player') && Input::get('review')) {
            $review         = new Review();
            $review->player = Input::get('player');
            $review->review = Input::get('review');
            $server->reviews()->save($review);
            
            Session::flash('success', Lang::get('server.messages.review.success'));
            return Redirect::route('server.view', array(
                $name,
                'reviews'
            ));
        }
        $wrong = true;
        Session::flash('wrong', true);
        return Redirect::route('server.view', array(
            $name,
            'reviews'
        ))->withInput();
    }
}