<?php
class BaseController extends Controller
{
    public function __construct()
    {
        $rotator = Premium::getPremiums(2, true);
        View::share('rotator', $rotator);
    }
    
    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout()
    {
        if (!is_null($this->layout)) {
            $this->layout = View::make($this->layout);
        }
    }
    
    private function getCountryByIp($ip)
    {
        $response = json_decode(file_get_contents('http://www.geoplugin.net/json.gp?ip=' . $ip));
        return $response["geoplugin_countryCode"];
    }
}