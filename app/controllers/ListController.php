<?php
class ListController extends BaseController
{
    public function search()
    {
    }
    
    public function view()
    {
        // Get filters
        $population = json_decode(urldecode(@$_COOKIE['filter_population']));
        $version    = json_decode(urldecode(@$_COOKIE['filter_version']));
        $keywords   = json_decode(urldecode(@$_COOKIE['filter_keywords']));
        $countries  = json_decode(urldecode(@$_COOKIE['filter_countries']));
        
        // Sorting 
        $sort     = urldecode(@$_COOKIE['sort']);
        $orderKey = 'votes';
        /* Default order */
        switch ($sort) {
            case 'votes':
                break;
            case 'score':
                $orderKey = 'score';
                break;
            case 'players':
                $orderKey = 'playercount';
                break;
            case 'created':
                $orderKey = 'created_at';
                break;
            case 'updated':
                $orderKey = 'updated_at';
                break;
            case 'uptime':
                $orderKey = 'uptime';
                break;
        }
        
        // Order Direction
        $direction = urldecode(@$_COOKIE['sort_direction']);
        if (!$direction)
            $direction = 'desc';
        
        /**
        Here come the worst query builders ever.
        Should probably convert to Eloquent, but that would be too much of a hassle, as it wouldn't really increase performance
        all that much. Review this code if or when it becomes a bottleneck (so, never).
        */
        // Build Population
        if ($population) {
            $num   = 0;
            $query = "";
            foreach ($population as $option) {
                if ($num > 0)
                    $query .= ' OR ';
                switch ($option) {
                    case "cozy":
                        $query .= 'playercount <= 50';
                        break;
                    case "big":
                        $query .= '(playercount > 50 AND playercount < 100)';
                        break;
                    case "massive":
                        $query .= 'playercount >= 100';
                        break;
                }
                $num = $num + 1;
            }
            $population = $query;
        } else {
            $population = '1=1';
        }
        
        // Build Version
        if ($version) {
            $num   = 0;
            $query = "";
            foreach ($version as $option) {
                if (!preg_match("/((\d+)\.(\d+)(\.(\d+))*)+|\d\dw\d\d[a-z]/i", $option)) {
                    // Invalid version, don't try mate
                } else {
                    if ($num === 0) {
                        $query .= "version='" . $option . "'";
                    } else {
                        $query .= " OR version='" . $option . "'";
                    }
                    $num = $num + 1;
                }
            }
            $version = $query;
        } else {
            $version = '1=1';
        }
        
        // Build countries
        if ($countries) {
            $num   = 0;
            $query = "";
            foreach ($countries as $option) {
                if (!preg_match("/[A-Z]{2}/", $option)) {
                    // Invalid version, don't try mate
                } else {
                    if ($num === 0) {
                        $query .= "country='" . $option . "'";
                    } else {
                        $query .= " OR country='" . $option . "'";
                    }
                    $num = $num + 1;
                }
            }
            $countries = $query;
        } else {
            $countries = '1=1';
        }
        
        // Build keywords
        if ($keywords && in_array('any', $keywords))
            $keywords = false;
        if ($keywords) {
            $servers = Server::with(array(
                'premium' => function($q)
                {
                    $q->where('created_at', '>=', DB::raw('NOW() - INTERVAL 1 MONTH'));
                }
            ), 'keywords')->whereRaw('(' . $version . ') AND (' . $population . ') AND (' . $countries . ')')->whereHas('keywords', function($q) use ($keywords)
            {
                $q->whereIn('keywords.id', $keywords);
            }, count($keywords))->orderBy($orderKey, $direction)->paginate(30);
        } else {
            $servers = Server::with(array(
                'premium' => function($q)
                {
                    $q->where('created_at', '>=', DB::raw('NOW() - INTERVAL 1 MONTH'));
                }
            ), 'keywords')->whereRaw('(' . $version . ') AND (' . $population . ') AND (' . $countries . ')')->orderBy($orderKey, $direction)->paginate(30);
        }
        
        // Get premiums
        $premiums = Premium::getPremiums(5);
		
        if (array_key_exists('ajax', $_GET))
            return View::make('ajax/list', array(
                'servers' => $servers,
                'premiums' => $premiums
            ));
        return View::make('list', array(
            'servers' => $servers,
            'premiums' => $premiums
        ));
    }
}