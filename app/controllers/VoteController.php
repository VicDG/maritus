<?php
class VoteController extends BaseController
{
    public function create($name)
    {
        $server = Server::name($name);
        $player = (Input::get('player') ? Input::get('player') : false);
        
        if (trim(file_get_contents('https://minecraft.net/haspaid.jsp?user=' . $player)) != "true") {
            Session::flash('danger', Lang::get('server.messages.vote.premium'));
            return Redirect::route('server.view', array(
                $name,
                'vote'
            ));
        }
        
        if ($server->addVote($player)) {
            Session::flash('success', Lang::get('server.messages.vote.success', array(
                'server' => $name
            )));
            return Redirect::route('server.view', array(
                $name
            ));
        } else {
            Session::flash('danger', Lang::get('server.messages.vote.fail'));
            return Redirect::route('server.view', array(
                $name,
                'vote'
            ));
        }
        
    }
}