<?php
use Carbon\Carbon;
use BaseController;

class ServerController extends BaseController
{
    protected $countries;
    
    private function getCountries()
    {
        if (!empty($this->countries))
            return $this->countries;
        $this->countries = json_decode(file_get_contents(app_path() . '/extras/countries.json'));
        return $this->countries;
    }
    
    /**
     * Creating new servers
     */
    public function add()
    {
        $countries = $this->getCountries();
        $ip        = Request::getClientIp();
        $country   = file_get_contents("http://ipinfo.io/{$ip}/country");
        return View::make('server/add', array(
            'countries' => $countries,
            'country' => $country
        ));
    }
    
    public function create()
    {
        // Create new server instance
        $server = new Server;
        
        if (Input::hasFile('banner_file')) {
            // Check for sizes
            list($width, $height) = getimagesize(Input::file('banner_file')->getRealPath());
            if (!($width === 468 && $height === 60)) {
                if (@$_GET['json'] == 1) {
                    return Response::json(array(
                        'status' => 'error',
                        'errors' => array(
                            'banner_file' => 'Your banner needs to be 468x60 pixels'
                        )
                    ));
                } else {
                    Session::flash('danger', Lang::get('server.messages.edit.error') . implode("<br/>", 'Your banner needs to be 468x60 pixels'));
                    return Redirect::route('server.edit', array(
                        $name
                    ))->withInput()->withErrors($server->errors());
                }
            }
        }
        
        // Did it succeed?
        if ($server->save()) {
            $server->setKeywords(Input::get('keywords'));
            $server->refresh();
            
            // Upload banner
            if (Input::hasFile('banner_file')) {
                // Remove current banner
                if (!empty($server->banner) && @file_exists(base_path() . (string) $server->banner)) {
                    unlink(base_path() . $server->banner);
                }
                
                $banner          = Input::file('banner_file');
                $destinationPath = 'uploads/banners';
                
                // Read & create properties
                $filename  = str_random(12);
                $extension = $banner->getClientOriginalExtension();
                
                // Set banner properties
                $location = '/' . 'public' . '/' . $destinationPath;
                $file     = $filename . '.' . $extension;
                $path     = $location . '/' . $file;
                
                // Save banner location
                $server->banner = '/' . $destinationPath . '/' . $file;
                $server->forceSave();
                
                // Move banner
                $banner = $banner->move(public_path() . '/' . $destinationPath, $file);
            }
            
            // Yes, query the server!
            $date = Carbon::now()->addMinutes(5);
            exec('/usr/bin/php '.base_path().'/artisan task:do banner '.$server->id);
            Queue::later($date, 'PollServer', array('id' => $server->id, 'env' => app()->env), 'online');
            
            Session::flash('success', Lang::get('server.messages.register.success'));
            if (@$_GET['json'] == 1) {
                return Response::json(array(
                    'status' => 'success',
                    'redirect' => route('login')
                ));
            } else {
                return Redirect::route('login');
            }
        }
        
        // Did not validate
        // Repopulate keywords
        if (Input::get('keywords')) {
            $keywords = Input::get('keywords');
            Session::flash('keywords', $keywords);
        }
        
        if (@$_GET['json'] == 1) {
            return Response::json(array(
                'status' => 'error',
                'errors' => $server->errors()->toArray()
            ));
        } else {
            Session::flash('danger', Lang::get('server.messages.register.error') . implode("<br/>", $server->errors()->all()));
            return Redirect::to('server/add')->withInput()->withErrors($server->errors());
        }
    }
    
    /**
     * Editing servers
     */
    public function edit($name)
    {
        $server    = Server::nameOrFail($name);
        $keywords  = ($server->getKeywords()) ? $server->getKeywords() : array();
        $countries = $this->getCountries();
        $ip        = Request::getClientIp();
        Session::flash('keywords', $server->getKeywords());
        return View::make('server/edit', array(
            'server' => $server,
            'countries' => $countries,
            'country' => $server->country
        ));
    }
    
    public function update($name)
    {
        $server = Server::nameOrFail($name);
        $server->setRules(true);
        unset($_POST['name']);
        // Did it succeed?
        if ($server->save()) {
            $server->setKeywords(Input::get('keywords'));
            $server->refresh();
            
            // Upload banner
            if (Input::hasFile('banner_file')) {
                list($width, $height) = getimagesize(Input::file('banner_file')->getRealPath());
                if ($width === 468 && $height === 60) {
                    // Remove current banner
                    if (!empty($server->banner) && @file_exists(base_path() . (string) $server->banner)) {
                        unlink(base_path() . $server->banner);
                    }
                    
                    $banner          = Input::file('banner_file');
                    $destinationPath = 'uploads/banners';
                    
                    // Read & create properties
                    $filename  = str_random(12);
                    $extension = $banner->getClientOriginalExtension();
                    
                    // Set banner properties
                    $location = '/' . 'public' . '/' . $destinationPath;
                    $file     = $filename . '.' . $extension;
                    $path     = $location . '/' . $file;
                    
                    // Save banner location
                    $server->banner = '/' . $destinationPath . '/' . $file;
                    $server->forceSave();
                    
                    // Move banner
                    $banner = $banner->move(public_path() . '/' . $destinationPath, $file);
                } else {
                    if (@$_GET['json'] == 1) {
                        return Response::json(array(
                            'status' => 'error',
                            'errors' => array(
                                'banner_file' => 'Your banner needs to be 468x60 pixels'
                            )
                        ));
                    } else {
                        Session::flash('danger', Lang::get('server.messages.edit.error') . implode("<br/>", 'Your banner needs to be 468x60 pixels'));
                        return Redirect::route('server.edit', array(
                            $name
                        ))->withInput()->withErrors($server->errors());
                    }
                }
            }
            
            Session::flash('success', Lang::get('server.messages.edit.success'));
            if (@$_GET['json'] == 1) {
                return Response::json(array(
                    'status' => 'success',
                    'redirect' => route('server.view', array(
                        $server->name
                    ))
                ));
            } else {
                return Redirect::route('server.view', array(
                    $server->name
                ));
            }
        }
        
        // Did not validate
        if (Input::get('keywords')) {
            $keywords = Input::get('keywords');
            Session::flash('keywords', $keywords);
        }
        
        if (@$_GET['json'] == 1) {
            return Response::json(array(
                'status' => 'error',
                'errors' => $server->errors()->toArray()
            ));
        } else {
            Session::flash('danger', Lang::get('server.messages.edit.error') . implode("<br/>", $server->errors()->all()));
            return Redirect::route('server.edit', array(
                $name
            ))->withInput()->withErrors($server->errors());
        }
    }
    
    
    /**
     * Deleting servers
     */
    public function delete($name)
    {
        $server = Server::nameOrFail($name);
        $wrong  = false;
        
        // Make view
        return View::make('server/delete', array(
            'server' => $server,
            'wrong' => $wrong
        ));
    }
    
    public function remove($name)
    {
        $server = Server::nameOrFail($name);
        $salt   = @$server->salt;
        
        if (Auth::validate(array(
            'name' => $name,
            'password' => Input::get('password') . $salt
        )) || Admin::inSession()) {
            $server->delete();
            Session::flash('success', Lang::get('server.messages.delete.success'));
            return Redirect::to('/');
        }
        
        // Set wrong to true
        $wrong = true;
        
        // Make view
        return View::make('server/delete', array(
            'server' => $server,
            'wrong' => $wrong
        ));
    }
    
    public function view($name, $tab = false, $ajax = false)
    {
        $server = Server::withCache($name);
        
        // Rotator
        $premium = false;
        if (@$server->premium->paid >= 20)
            $premium = true;
        
        // BBCode
        require_once(app_path() . '/lib/Nbbc.php');
        $bbcode = new BBCode;
        
        // Which tabs are to be included?
        $tabs = Array(
            'description',
            'players',
            'banners',
            'reviews',
            'vote'
        );
        if ((!Auth::check() || (Auth::user()->name != $server->name)) && !Admin::inSession())
            $tabs[] = 'login';
        
        // Is it an ajax call?
        if ($ajax == 'ajax')
            return View::make('server/view/' . $tab, array(
                'server' => $server,
                'bbcode' => $bbcode
            ));
        
        // It's not
        $args = array(
            'server' => $server,
            'tab' => $tab,
            'tabs' => $tabs,
            'bbcode' => $bbcode
        );
        if ($premium === true)
            $args['rotator'] = null;
        return View::make('server/view', $args);
    }
    
    /**
     * Reset password
     */
    public function reset()
    {
        return View::make('server/reset/request', Array(
            'wrong' => false
        ));
    }
    
    public function requestReset()
    {
        if (Input::get('email') && Input::get('name')) {
            $server = Server::where('email', '=', Input::get('email'))->where('name', '=', Input::get('name'))->get();
            $wrong  = false;
            if (!$server->isEmpty()) {
                $server = $server->first();
                $token  = $server->reset();
                $id = $server->id;
                
                	Mail::send('mail/reset', array(
                	    'name' => $server->name,
                	    'id' => $server->id,
                	    'token' => $token
                	), function($message) use ($server)
                	{
                	    $message->to($server->email)->subject(Lang::get('server.strings.password_reset'));
                	});
                
                Session::flash('success', Lang::get('server.messages.reset.sent'));
                return Redirect::to('/');
            }
        }
        return View::make('server/reset/request', Array(
            'wrong' => true
        ));
    }
    
    public function choosePassword($id, $token)
    {
        $server = Server::find($id);
        return View::make('server/reset/choose', array(
            'server' => $server,
            'token' => $token,
            'id' => $id
        ));
    }
    
    public function setPassword($id, $token)
    {
        $server = Server::find($id);
        $server->setRules('reset');
        if ($server->save()) {
            $server->reset = null;
            $server->forceSave();
            Session::flash('success', Lang::get('server.messages.reset.success'));
            return Redirect::route('login');
        }
        return View::make('server/reset/choose', array(
            'server' => $server,
            'token' => $token,
            'id' => $id
        ))->withErrors($server->errors());
    }
}
