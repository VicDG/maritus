<?php
class Bean {
    public static function push($job, $data = '', $queue = null, $priority = 1024)
	{
	    $pheanstalk = Queue::getPheanstalk();
        return $pheanstalk->useTube($queue)->put(
            json_encode(array('job' => $job, 'data' => $data)), $priority, $pheanstalk::DEFAULT_DELAY, 500
        );
	} 
	
	public static function later($delay, $job, $data = '', $queue = null, $priority = 1024) {
		if ($delay instanceof DateTime)
		{
			$delay = max(0, $delay->getTimestamp() - $this->getTime());
		}
		else
		{
			$delay = intval($delay);
		}
		
        $payload = json_encode(array('job' => $job, 'data' => $data));
    
		$pheanstalk = Queue::getPheanstalk();
		$pheanstalk = $pheanstalk->useTube($queue);

		return $pheanstalk->put($payload, $priority, $delay, 500);
	}
}