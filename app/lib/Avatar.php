<?php
class Avatar
{
    public static function url($player, $size = 64)
    {
        //return 'http://avatar4.yourminecraftservers.com/avatar/rad/steve/round/' . $size . '/' . $player . '.png';
        //return 'http://aeolus.minequery.net/?p=' . $player . '&s=' . $size;
        return URL::to('/avatar/' . $player . '/' . $size);
    }
}
