<?php
class Favicon {
	private static function getData($id) 
	{
		$server = Server::find($id);
		$data = $server->cache->favicon;
		if(!empty($data)) {
			return $data;
		}
		return false;
	}
	
	public static function save($id) 
	{
		$data = self::getData($id);
		if(!$data) return false;

		list($type, $data) = explode(';', $data);
		list(, $data)      = explode(',', $data);
		$data = base64_decode($data);	
		
		file_put_contents(public_path() . '/' . 'favicon_cache' . '/' . $id . '.png', $data);
		return true;
	}
}