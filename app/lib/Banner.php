<?php

class Banner
{
    public static function cache($server)
    {
        $server = Server::find($server);
        self::small($server);
        self::big($server);  

    }
    
    
    public static function location($server, $function, $start = false)
    {
        $dir = $start . '/' . 'banner_cache' . '/' . $function;
        if (!is_dir($dir) && $start !== false)
            mkdir($dir);
        return $dir . '/' . $server->name . '_' . $server->id . '.png';
    }
    
    /**
     * Small banner
     */
    public static function small($server)
    {
        // Paths
        $basepath = app_path() . '/' . 'banners';
        $mcpath   = $basepath . '/' . 'mc';
        
        // Background
        $background = $mcpath . '/' . 'background.png';
        
        // Open file
        $image = imagecreatefrompng($background);
        
        // Set fonts
        // White
        $white        = imagecolorallocate($image, 255, 255, 255);
        $white_shadow = imagecolorallocate($image, 128, 128, 128);
        
        // Gray-ish
        $gray        = imagecolorallocate($image, 170, 170, 170);
        $gray_shadow = imagecolorallocate($image, 42, 42, 42);
        
        // Dark
        $dark  = imagecolorallocatealpha($image, 255, 255, 255, 100);
        $black = imagecolorallocate($image, 0, 0, 0);
        
        // Darken
        $transblack = imagecolorallocatealpha($image, 70, 49, 33, 80);
        imagefilledrectangle($image, 0, 0, imagesx($image), imagesy($image), $transblack);
        
        // Red
        $red = imagecolorallocate($image, 170, 0, 10);
        
        // Font path
        putenv('GDFONTPATH=' . $basepath . '/' . 'font');
        
        // Choose font
        $mc = "mc";
        
        // Set font-sizes
        $fonts = 12;
        $mfont = 12 - 2.2;
        
        $snx = 15;
        $sny = 21;
        
        $mx = $snx;
        $my = $sny + 16;
        
        $sipx = $snx;
        $sipy = $my + 15;
        
        // Determine status
        if ($server->isOnline()) {
            $motd = $server->getCache('motd');
            $motd = (strlen($motd) > 43) ? substr($motd, 0, 40) . '...' : $motd;
            
            // Server name
            imagettftext($image, $fonts, 0, $snx + 1, $sny + 1, $white_shadow, $mc, $server->name);
            imagettftext($image, $fonts, 0, $snx, $sny, $white, $mc, $server->name);
            
            // Motd
            imagettftext($image, $mfont, 0, $mx + 1, $my + 1, $gray_shadow, $mc, $motd);
            imagettftext($image, $mfont, 0, $mx, $my, $gray, $mc, $motd);
            
            // Server IP
            //imagettftext($image, 12, 0, 16, 66, $black, $mc, $server->ip);
            imagettftext($image, $mfont, 0, $sipx, $sipy, $dark, $mc, $server->ip);
            
            // Players / Max
            $dimensions = imagettfbbox(12, 0, $mc, "{$server->getCache('players')->count}/{$server->getCache('players')->max}");
            $textWidth  = abs($dimensions[4] - $dimensions[0]);
            $x          = imagesx($image) - $textWidth - 12;
            imagettftext($image, 12, 0, $x + 1, 46, $gray_shadow, $mc, "{$server->getCache('players')->count}/{$server->getCache('players')->max}");
            imagettftext($image, 12, 0, $x, 45, $gray, $mc, "{$server->getCache('players')->count}/{$server->getCache('players')->max}");
            
            // Ping
            $ping_path = $mcpath . '/' . 'bars' . '/' . 'bars_5.png';
            $ping_img  = imagecreatefrompng($ping_path);
            imagecopy($image, $ping_img, imagesx($image) - imagesx($ping_img) - 16, 10, 0, 0, imagesx($ping_img), imagesy($ping_img));
        } else {
            // Server name
            imagettftext($image, $fonts, 0, $snx + 1, $sny + 1, $white_shadow, $mc, $server->name);
            imagettftext($image, $fonts, 0, $snx, $sny, $white, $mc, $server->name);
            
            // Motd
            $motd = "Can't reach server";
            imagettftext($image, $mfont, 0, $mx + 1, $my + 1, $gray_shadow, $mc, $motd);
            imagettftext($image, $mfont, 0, $mx, $my, $red, $mc, $motd);
            
            // Server IP
            //imagettftext($image, 12, 0, 16, 66, $black, $mc, $server->ip);
            imagettftext($image, $mfont, 0, $sipx, $sipy, $dark, $mc, $server->ip);
            
            // Ping
            $ping_path = $mcpath . '/' . 'bars' . '/' . 'bars_0.png';
            $ping_img  = imagecreatefrompng($ping_path);
            imagecopy($image, $ping_img, imagesx($image) - imagesx($ping_img) - 16, 10, 0, 0, imagesx($ping_img), imagesy($ping_img));
        }
        
        // Save
        imagepng($image, self::location($server, __FUNCTION__, public_path()));
        
        // Cleanup
        imagedestroy($image);
        
        return true;
    }
    
    /**
     * Small banner
     */
    public static function big($server)
    {
        // Paths
        $basepath = app_path() . '/' . 'banners';
        $mcpath   = $basepath . '/' . 'mc';
        
        // Background
        $background = $mcpath . '/' . 'big_background.png';
        
        // Open file
        $image = imagecreatefrompng($background);
        
        // Set fonts
        // White
        $white        = imagecolorallocate($image, 255, 255, 255);
        $white_shadow = imagecolorallocate($image, 128, 128, 128);
        
        // Gray-ish
        $gray        = imagecolorallocate($image, 170, 170, 170);
        $gray_shadow = imagecolorallocate($image, 42, 42, 42);
        
        // Dark
        $dark  = imagecolorallocatealpha($image, 255, 255, 255, 100);
        $black = imagecolorallocate($image, 0, 0, 0);
        
        // Darken
        $transblack = imagecolorallocatealpha($image, 70, 49, 33, 80);
        imagefilledrectangle($image, 0, 0, imagesx($image), imagesy($image), $transblack);
        
        // Red
        $red = imagecolorallocate($image, 170, 0, 10);
        
        // Font path
        putenv('GDFONTPATH=' . $basepath . '/' . 'font');
        
        // Choose font
        $mc = "mc";
        
        // Set font-sizes
        $fonts = 12;
        $mfont = 12;
        
        $snx = 15;
        $sny = 25;
        
        $mx = $snx;
        $my = $sny + 20;
        
        $sipx = $snx;
        $sipy = $my + 20;
        
        if ($server->getCache('favicon')) {
            $snx  = $snx + 70;
            $mx   = $mx + 70;
            $sipx = $sipx + 70;
            
            // Get favicon
            $favicon = imagecreatefromstring(base64_decode(str_replace('data:image/png;base64,', '', $server->getCache('favicon'))));
            
            // Put favicon
            imagecopy($image, $favicon, 10, 10, 0, 0, imagesx($favicon), imagesy($favicon));
        }
        
        // Determine status
        if ($server->isOnline()) {
            $motd = $server->getCache('motd');
            if ($server->getCache('favicon')) {
                $motd = (strlen($motd) > 33) ? substr($motd, 0, 30) . '...' : $motd;
            } else {
                $motd = (strlen($motd) > 43) ? substr($motd, 0, 40) . '...' : $motd;
            }
            
            
            // Server name
            imagettftext($image, $fonts, 0, $snx + 1, $sny + 1, $white_shadow, $mc, $server->name);
            imagettftext($image, $fonts, 0, $snx, $sny, $white, $mc, $server->name);
            
            // Motd
            imagettftext($image, $mfont, 0, $mx + 1, $my + 1, $gray_shadow, $mc, $motd);
            imagettftext($image, $mfont, 0, $mx, $my, $gray, $mc, $motd);
            
            // Server IP
            //imagettftext($image, 12, 0, 16, 66, $black, $mc, $server->ip);
            imagettftext($image, $mfont, 0, $sipx, $sipy, $dark, $mc, $server->ip);
            
            // Players / Max
            $dimensions = imagettfbbox(12, 0, $mc, "{$server->getCache('players')->count}/{$server->getCache('players')->max}");
            $textWidth  = abs($dimensions[4] - $dimensions[0]);
            $x          = imagesx($image) - $textWidth - 12;
            imagettftext($image, 12, 0, $x + 1, 46, $gray_shadow, $mc, "{$server->getCache('players')->count}/{$server->getCache('players')->max}");
            imagettftext($image, 12, 0, $x, 45, $gray, $mc, "{$server->getCache('players')->count}/{$server->getCache('players')->max}");
            
            // Ping
            $ping_path = $mcpath . '/' . 'bars' . '/' . 'bars_5.png';
            $ping_img  = imagecreatefrompng($ping_path);
            imagecopy($image, $ping_img, imagesx($image) - imagesx($ping_img) - 16, 10, 0, 0, imagesx($ping_img), imagesy($ping_img));
        } else {
            // Server name
            imagettftext($image, $fonts, 0, $snx + 1, $sny + 1, $white_shadow, $mc, $server->name);
            imagettftext($image, $fonts, 0, $snx, $sny, $white, $mc, $server->name);
            
            // Motd
            $motd = "Can't reach server";
            imagettftext($image, $mfont, 0, $mx + 1, $my + 1, $gray_shadow, $mc, $motd);
            imagettftext($image, $mfont, 0, $mx, $my, $red, $mc, $motd);
            
            // Server IP
            //imagettftext($image, 12, 0, 16, 66, $black, $mc, $server->ip);
            imagettftext($image, $mfont, 0, $sipx, $sipy, $dark, $mc, $server->ip);
            
            // Ping
            $ping_path = $mcpath . '/' . 'bars' . '/' . 'bars_0.png';
            $ping_img  = imagecreatefrompng($ping_path);
            imagecopy($image, $ping_img, imagesx($image) - imagesx($ping_img) - 16, 10, 0, 0, imagesx($ping_img), imagesy($ping_img));
        }
        
        // Save
        imagepng($image, self::location($server, __FUNCTION__, public_path()));
        
        // Cleanup
        imagedestroy($image);
        
        return true;
    }
}