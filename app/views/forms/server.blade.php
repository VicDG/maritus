<div id="errors_container" class="alert alert-danger hidden">
  <ul id="errors">
  </ul>
</div>      
<div class="well">
  <h2>{{ Lang::get('server.strings.your_server') }}</h2>
  <hr/>
  <div id="preload" class="preload-container hidden">
    <img class="preload centered" alt="preloader" src="{{ URL::to('img/preload.gif') }}"/>
  </div>
  @if (Route::currentRouteName() == 'server.add')
    {{ Form::open(array('route' => 'server.create', 'class' => 'form-horizontal center', 'id' => 'server', 'role' => 'form', 'files' => true)) }}
  @else
    {{ Form::model($server, array('route' => array('server.update', $server->name), 'class' => 'form-horizontal center', 'id' => 'server', 'role' => 'form', 'files' => true)) }}
  @endif
    <h4>{{ Lang::get('server.strings.register_titles.email') }}</h4>
    <hr/>
    <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
      {{ Form::label('email', Lang::get('server.strings.email'), array('class' => 'control-label col-md-4')) }}
      <div class="col-md-4">
        {{ Form::text('email', null, array('class' => 'form-control', 'id' => 'email')) }}
        {{ $errors->first('email', '<br/><div class="alert alert-danger">:message</div>') }}
      </div>
    </div>
    <hr/>
    <h4>{{ Lang::get('server.strings.register_titles.password') }}</h4>
    <hr/>
    <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
      {{ Form::label('password', Lang::get('server.strings.password'), array('class' => 'control-label col-md-4')) }}
      <div class="col-md-3">
        {{ Form::password('password', array('class' => 'form-control', 'id' => 'password')) }}
      </div>
    </div>
    <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
      {{ Form::label('password_confirmation', Lang::get('server.strings.retype_password'), array('class' => 'control-label col-md-4')) }}
      <div class="col-md-3">
        {{ Form::password('password_confirmation', array('class' => 'form-control', 'id' => 'password_confirmation')) }}
        {{ $errors->first('password', '<br/><div class="alert alert-danger">:message</div>') }}
      </div>
    </div>
    <hr/>
    <h4>{{ Lang::get('server.strings.register_titles.information') }}</h4>
    <hr/>
    @if (Route::currentRouteName() == 'server.add')
      <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
        {{ Form::label('name', Lang::get('server.strings.server_name'), array('class' => 'control-label col-md-4')) }}
        <div class="col-md-3">
          {{ Form::text('name', null, array('class' => 'form-control', 'id' => 'name')) }}
          {{ $errors->first('name', '<br/><div class="alert alert-danger">:message</div>') }}
          <p class="help-block">{{ Lang::get('server.messages.register.help.name') }}</p>
        </div>
      </div>
    @endif
    <div class="form-group {{ $errors->has('website') ? 'has-error' : '' }}">
      {{ Form::label('website', Lang::get('server.strings.website'), array('class' => 'control-label col-md-4')) }}
      <div class="col-md-4">
        <div class="input-group">
        <span class="input-group-addon">http://</span>
        {{ Form::text('website', null, array('class' => 'form-control', 'id' => 'website')) }}
        {{ $errors->first('website', '<br/><div class="alert alert-danger">:message</div>') }}
        </div>
      </div>
    </div>
    <div class="form-group {{ $errors->has('banner_file') ? 'has-error' : '' }}">
      {{ Form::label('banner_file', Lang::get('server.strings.upload_banner'), array('class' => 'control-label col-md-4')) }}
      <div class="col-md-3">
        {{ Form::file('banner_file', null, array('class' => 'form-control', 'id' => 'banner')) }}
        {{ $errors->first('banner', '<br/><div class="alert alert-danger">:message</div>') }}
      </div>
    </div>
    <div class="form-group {{ $errors->has('country') ? 'has-error' : '' }}">
      {{ Form::label('country', Lang::get('server.strings.server_country'), array('class' => 'control-label col-md-4')) }}
      <div class="col-md-3">
        {{ Form::select('country', $countries, trim($country), array('class' => 'form-control', 'id' => 'country')) }}
        {{ $errors->first('country', '<br/><div class="alert alert-danger">:message</div>') }}
      </div>
    </div>
    <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
      {{ Form::label('description', Lang::get('server.strings.description'), array('class' => 'control-label col-md-4')) }}
      <div class="col-md-6">
        {{ Form::textarea('description', null, array('class' => 'form-control', 'id' => 'description')) }}
        {{ $errors->first('description', '<br/><div class="alert alert-danger">:message</div>') }}
      </div>
    </div>
    <hr/>
    <h4>{{ Lang::get('server.strings.register_titles.connect') }}</h4>
    <hr/>
    <div class="form-group {{ $errors->has('ip') ? 'has-error' : '' }}">
      {{ Form::label('ip', 'IP', array('class' => 'control-label col-md-offset-3 col-md-1')) }}
      <div class="col-md-3">
        {{ Form::text('ip', null, array('class' => 'form-control', 'id' => 'ip')) }}
        {{ $errors->first('ip', '<br/><div class="alert alert-danger">:message</div>') }}
        <p class="help-block">{{ Lang::get('server.messages.register.help.ip') }}</p>
      </div>
      {{ Form::label('port', Lang::get('server.strings.port'), array('class' => 'control-label col-md-1')) }}
      <div class="col-md-2">
        {{ Form::text('port', null, array('class' => 'form-control', 'placeholder' => '25565', 'id' => 'port')) }}
        {{ $errors->first('port', '<br/><div class="alert alert-danger">:message</div>') }}
        <p class="help-block">{{ Lang::get('server.messages.register.help.port') }}</p>
      </div>
    </div>
    <div class="form-group {{ $errors->has('query') ? 'has-error' : '' }}">
      {{ Form::label('query', Lang::get('server.strings.query_port'), array('class' => 'control-label col-md-4 col-md-offset-4')) }}
      <div class="col-md-2">
        {{ Form::text('query', null, array('class' => 'form-control', 'id' => 'query')) }}
        {{ $errors->first('query', '<br/><div class="alert alert-danger">:message</div>') }}
        <p class="help-block">{{ Lang::get('server.messages.register.help.query') }}</p>
      </div>
    </div>
    <hr/>
    <h4>{{ Lang::get('server.strings.register_titles.votifier') }}</h4>
    <hr/>
    <div class="form-group {{ $errors->has('votifier_ip') ? 'has-error' : '' }}">
      {{ Form::label('votifier_ip', 'Votifier IP', array('class' => 'control-label col-md-offset-3 col-md-1')) }}
      <div class="col-md-3">
        {{ Form::text('votifier_ip', null, array('class' => 'form-control', 'id' => 'votifier_ip')) }}
      </div>
      {{ Form::label('votifier_port', Lang::get('server.strings.port'), array('class' => 'control-label col-md-1')) }}
      <div class="col-md-2">
        {{ Form::text('votifier_port', null, array('class' => 'form-control', 'placeholder' => '25565', 'id' => 'votifier_port')) }}
      </div>
    </div>
    <div class="form-group {{ $errors->has('votifier_key') ? 'has-error' : '' }}">
      {{ Form::label('votifier_key', 'Public Key', array('class' => 'control-label col-md-4')) }}
      <div class="col-md-6">
        {{ Form::textarea('votifier_key', null, array('class' => 'form-control', 'id' => 'votifier_key')) }}
        {{ $errors->first('votifier_ip', '<br/><div class="alert alert-danger">:message</div>') }}
        {{ $errors->first('votifier_key', '<br/><div class="alert alert-danger">:message</div>') }}
        {{ $errors->first('votifier_port', '<br/><div class="alert alert-danger">:message</div>') }}
      </div>
    </div>
    <hr/>
    <h4>{{ Lang::get('server.strings.register_titles.keywords') }}</h4>
    <hr/>
    
    <div data-toggle="buttons">
        @foreach (Keyword::all() as $keyword)
          <label style="margin-bottom: 5px;" class="btn btn-info {{ (@in_array($keyword->id, Session::get('keywords'))) ? 'active' : '' }}">
            <input name="keywords[]" type="checkbox" value="{{ $keyword->id }}"  {{ (@in_array($keyword->id, Session::get('keywords'))) ? 'checked' : '' }}> {{ $keyword->name }}
          </label>
        @endforeach
      </div>
    
    <hr/>
    <h4>{{ Lang::get('server.strings.register_titles.all') }}</h4>
    <hr/>
    <div class="form-group">
      <div class="col-md-8">
        <small>{{ Lang::get('server.messages.register.tos_warn') }}</small>
      </div>
      <div class="col-md-4">
         {{ link_to_route('page', Lang::get('server.strings.tos'), array('page' => 'tos'), array('class' => 'btn btn-primary btn-lg btn-warning', 'target' => 'blank')) }}
         {{ Form::submit(Lang::get('server.strings.submit'), array('class' => 'btn btn-primary btn-lg btn-success')) }}
      </div>
    </div>
  {{ Form::close() }}
</div> 
@section('styles')
  <style>
    .is_stuck {
      margin-top: 20px;
    }
  </style>
@stop

@section('scripts')
  <script>
    $(document).ready(function() {
      var wbbOpt = {
        lang : 'en',
        buttons : 'bold,italic,underline,|,img,link,|,bullist,numlist,|,justifyleft,justifycenter,justifyright,|,removeFormat'
      }
      $('#description').wysibb(wbbOpt);
      $('.captcha').captcha({
        autoSubmit: true,
        autoRevert: true,
        txtLock: '',
        txtUnlock: '',
        img_uploadurl: '/editor/upload' 
      });
    });
  </script>
@stop
