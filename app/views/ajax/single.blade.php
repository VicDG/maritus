<div class="clear server-visible-small" style="height: 30px;"></div>
<a  target="_blank" href="{{ route('server.view', array($server->name)) }}">
	<div class="server {{ (@$server->premium->exists && $server->premium->paid >= 5) ? 'premium' : '' }}">
		@if (count($server->keywords))
		<div class="keywords">
			@foreach ($server->keywords as $keyword)
			<div class="keyword">
				{{ $keyword->name }}
			</div>
			@endforeach
		</div>
		<div class="clear"></div>
		@endif
		<div class="info">
			<div class="description">
				<div class="favicon">
					<img alt="{{ $server->name }} {{ SERVERLIST_GAME }} Server Favicon" src="{{ route('favicon', $server->name) }}" >
				</div>
				<div class="intro">
				  <span>{{{ str_limit(preg_replace(array('/\[[^\]\[]*\]/', '/(http|https):\/\/[^\s]*/'), '', $server->description), $limit = 110, $end = '...') }}}</span>
				</div>
			</div>
			<div class="banner">
	    		@if (!empty($server->banner) && @$server->premium->exists)
	    		  <img alt="{{ $server->name }} Minecraft Server Banner" src="{{ $server->banner }}">
	    		@else
	    		  <img alt="{{ $server->name }} Dynamic Minecraft Server Banner" src="{{ route('banner.small', $server->name) }}">
	    		@endif
			</div>
			<div class="data">
				<div class="score light left">
	    			<span class="glyphicon glyphicon-fire"></span>
					{{ $server->roundScore() }}
				</div>
				<div class="uptime right">
					<div style="width: {{ ($server->uptime < 60) ? '100' : $server->uptime }}%;" class="slider {{ $server->statusBar() }}">
	    				{{ $server->uptime }}%
					</div>
				</div>
				<div class="votes left">
					<span class="glyphicon glyphicon-check"></span>
					{{ $server->votes }}
				</div>
				<div class="players light right">
					{{ ($server->playermax) ? $server->playercount . ' / ' . $server->playermax : '0 / 0' }}
				</div>
			</div>
		</div>
		<div class="general">
			<div class="ranking">
				<div class="number light">
					{{ (isset($num)) ? '#' . $num : '<span class="glyphicon glyphicon-certificate"></span>'  }}
				</div>
				<div class="name">
					{{ str_limit($server->name, $limit = 20, $end = '...') }}
				</div>
			</div>
			<br class="server-visible-small" >
			<div class="connection">
				<div class="status {{ ((bool)@$server->cache->status) ? 'online' : 'offline' }}">
				  {{ ((bool)@$server->cache->status) ? 'Online' : 'Offline' }}
				</div>
				<div class="version light">
				  {{ $server->version }}
				</div>
				<div class="ip">
				  {{ $server->ip }}{{ (!empty($server->port) && $server->port != '25565') ? ':' . $server->port : '' }}
				</div>
			</div>
			<div class="light country">
				<img alt="Country Flag" src="blank.gif" class="flag flag-{{ strtolower($server->country) }}" >
			</div>
			@if(@$server->premium->exists)
			<div class="premium">
				<span class="glyphicon glyphicon-certificate"></span>
				Premium
			</div>
			@endif
		</div>
	</div>
</a>