@if($servers->getCurrentPage() > 1 && count($servers))
  <div style="-webkit-border-top-left-radius: 0;-moz-border-radius-topright: 0;-moz-border-radius-topleft: 0;border-top-right-radius: 0;border-top-left-radius: 0; margin-bottom: 5px;" id="premiumtop">
    <h1>{{ Lang::get('content.list.titles.premium') }}</h1>
  </div>
  <!-- Premiums -->
  @include('ajax/premium')
  <!-- / Premiums -->
  <div style="min-height: 0;border: none;height: 45px;" id="votetop">
  	<h1>{{ SERVERLIST_NAME }}</h1>
  </div>
@endif
@if (count($servers))
  <div class="clear" style="height: 3px;"></div>
@endif
<?php $it = 1 ?>
@foreach ($servers as $key => $server)
  <?php $num = (($servers->getCurrentPage() - 1)* $servers->getPerPage()) + $it; ?>
  @include('ajax/single')
  <?php $it = $it + 1; ?> 
@endforeach
@if (!count($servers) && !array_key_exists('ajax', $_GET))
    <center>There were no servers matching your query.</center>
@endif
