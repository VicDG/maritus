<html>
  <head>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.1/css/font-awesome.css" rel="stylesheet">
    <style>
      .popover {
        -webkit-box-shadow: none;
        box-shadow: none;
      }
    </style>
  </head>
  <body style="background-color: transparent;">
    <div class="btn-group" id="popvotes" data-toggle="popover" data-trigger="manual" data-placement="right" data-content="{{ $server->votes()->count() }}" data-original-title="" title="" style="margin-top: 5px">
      {{ link_to_route('server.view', SERVERLIST_NAME, array($server->name, 'vote'), array('class' => 'btn btn-primary', 'target' => '_blank')) }}
      <a href="{{URL::route('server.view', array($server->name, 'vote'))}}" class="btn btn-info dropdown-toggle" style="height:34px;" target="_blank"><i style="margin-top: 2.5px;" class="fa fa-thumbs-o-up"></i></a>
    </div>
    {{ HTML::script('js/jquery-1.10.2.min.js') }}
    {{ HTML::script('js/bootstrap.min.js') }}
    <script>var $j = jQuery.noConflict();</script>
    <script type="text/javascript">
      <!--
        $j(window).load(function () {
        $j('#popvotes').popover('show');
        });
      //-->
    </script>
  </body>
</html>