<html>
  <head>
    <meta name="robots" content="noindex">
    {{ HTML::script('js/jquery-1.10.2.min.js') }}
    {{ HTML::script('js/chart/highstock.js') }}
    <script>
     $(function() {
      $.getJSON("{{ URL::route('api.players', array($server->name)) }}", function(data) {
        // Create the chart
        window.chart = new Highcharts.StockChart({
          chart : {
            renderTo : 'container',
            height : '{{ $height }}'
          },
          
          lang: {
            months: ['Januari', 'Februari', 'Maart', 'April', 'Mei', 'Juni', 
      'Juli', 'Augustus', 'September', 'October', 'November', 'December'],
            weekdays: ['Maandag', 'Dinsdag', 'Woensdag', 'Donderdag', 'Vrijdag', 'Zaterdag', 'Zondag']
          },
          
          yAxis : {
            gridLineWidth: 0,
            labels : {
              enabled: false
            },
            tickInterval : 1
          },
          
          rangeSelector : {
            selected : 1
          },
          
          scrollbar: {
            enabled: false
          },

          navigator : {
            enabled : false
          },

          credits : {
            text: "{{ $_SERVER['HTTP_HOST'] }}",
            href: "{{ URL::to('/') }}"
          },
          
          title : {
            enabled: false
          },
          
          rangeSelector: {
            buttons: [{
              type: 'day',
              count: 1,
              text: '1d'
            }, {
              type: 'month',
              count: 1,
              text: '1m'
            }],
            inputEnabled: false,
            buttonSpacing: 5,
            selected: 0
          },
          
          
          series : [{
            name : 'Players',
            data : data,
            tooltip: {
              valueDecimals: 0
            }
          }]
        });
      });
    
    });
    </script>
  </head>
  <body>
    <div id="container" style="height: {{ $height }}px; width: 100%"></div>
  </body>
</html>
