(function() {
  /* Create Elements */
  var element = document.createElement('div'); 
  element.setAttribute('class', 'playerlist');
  element.innerHTML = '{{ $content }}';
  
  var style = document.createElement('style');
  style.innerHTML = '{{ $style }}';
    
  /* Get script tag currently executing this function */
  var scripts = document.getElementsByTagName('script');
  var thisScript = scripts[scripts.length-1];
  
  /* Insert Scripts */
  thisScript.parentNode.insertBefore(style, thisScript);
  thisScript.parentNode.insertBefore(element, thisScript);
  
  /* Remove executing script tag */
  thisScript.parentNode.removeChild(thisScript);
})();
