@extends('layout')

@section('title')
  {{ SERVERLIST_NAME }} - Admin
@stop

@section('content')
<div class="well">
  <h2>{{ Lang::get('server.strings.login') }}</h2>
  <hr/>
  @if ($wrong === true)
    <div class="alert alert-danger">{{ Lang::get('server.messages.login.incorrect') }}</div>
    <hr/>
  @endif
  {{ Form::open(array('class' => 'form-horizontal', 'role' => 'form')) }}
    <div class="form-group {{ ($wrong === true) ? 'has-error' : '' }}">
      {{ Form::label('name', Lang::get('server.strings.name'), array('class' => 'control-label col-md-4')) }}
      <div class="col-md-4">
        {{ Form::text('name', '', array('class' => 'form-control')) }}
      </div>
    </div>
    <div class="form-group {{ ($wrong === true) ? 'has-error' : '' }}">
      {{ Form::label('password', Lang::get('server.strings.password'), array('class' => 'control-label col-md-4')) }}
      <div class="col-md-4">
        {{ Form::password('password', array('class' => 'form-control')) }}
      </div>
    </div>
    <hr/>
     <div class="form-group">
       <div class="col-md-offset-7 col-md-4">
          {{ Form::submit(Lang::get('server.strings.login'), array('class' => 'btn btn-primary btn-lg btn-success')) }}
       </div>
    </div>
  {{ Form::close() }}
</div>
@stop