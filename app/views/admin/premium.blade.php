@extends('layout')

@section('title')
  {{ SERVERLIST_NAME }} - Admin
@stop

@section('content')
<div class="well">
  <h2>Admin Premium Edit</h2>
  <hr/>
  {{ Form::open(array('class' => 'form-horizontal', 'role' => 'form')) }}
    <div class="form-group">
      {{ Form::label('paid', 'Amount', array('class' => 'control-label col-md-4')) }}
      <div class="col-md-4">
        {{ Form::text('paid', ($premium) ? $premium->paid : null, array('class' => 'form-control')) }}
      </div>
    </div>
    <hr/>
     <div class="form-group">
       <div class="col-md-offset-7 col-md-4">
          {{ Form::submit('Submit', array('class' => 'btn btn-primary btn-lg btn-success')) }}
       </div>
    </div>
  {{ Form::close() }}
</div>
@stop