@extends('layout')
@section('content')
<div class="well">
  <h2>{{ Lang::get('server.strings.tos') }}</h2>
  <hr/>
  {{ Lang::get('page.tos', array('serverlist_name' => SERVERLIST_NAME, 'serverlist_game' => SERVERLIST_GAME)) }}
</div>
@stop
@section('title')
  {{ SERVERLIST_NAME }} - {{ Lang::get('server.strings.tos') }}
@stop