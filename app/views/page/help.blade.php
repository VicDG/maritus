@extends('layout')
@section('content')
<div class="well">
  <h2>{{ Lang::get('server.strings.help') }}</h2>
  <hr/>
<div class="alert alert-info">
<p>Welcome to the new Minequery.</p>

<p>If you were using one of our API features like the playerlist, please re-embed the new codes that you can find on your server page. You can find the new version of the PHP API on <a href="https://github.com/Moeflon/minequery-api">GitHub</a>.</p>

<p>Passwords were not migrated because of security measures, please <b>reset your password at <a href="http://minequery.net/reset/request">http://minequery.net/reset/request</a></b> (accounts no longer exist, servers are seperate to make things easier, your server-name is the name that you can see in the link to your server, use dashes instead of spaces)<p>Urgent bugs that may be present because of the migration can be reported on Skype @ kiwistruik or via mail (support@minequery.net)</p>
</div>
  {{ Lang::get('page.help', array('serverlist_name' => SERVERLIST_NAME)) }}
</div>
@stop
@section('title')
  {{ SERVERLIST_NAME }} - {{ Lang::get('server.strings.help') }}
@stop