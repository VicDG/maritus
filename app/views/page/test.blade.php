@extends('layout')
@section('content')

<div class="server ">
				<div class="info">
			<div class="description">
				<div class="favicon">
					<img src="https://minequery.net/favicon/Poke-Planet-Pixelmon.png">
				</div>
				<div class="intro">
				  <span>Poke-Planet Pixelmon is a survival Pixelmon server with tons of great features.  We have custom plugins, great...</span>
				</div>
			</div>
			<div class="banner">
	    			    		  <img src="https://minequery.net/banner/Poke-Planet-Pixelmon.png">
	    					</div>
			<div class="data">
				<div class="score light left">
	    			<span class="glyphicon glyphicon-fire"></span>
					919				</div>
				<div class="uptime right">
					<div style="width: 100.00%;" class="slider good">
	    				100.00%
					</div>
				</div>
				<div class="votes left">
					<span class="glyphicon glyphicon-check"></span>
					273				</div>
				<div class="players light right">
					51 / 75				</div>
			</div>
		</div>
		<div class="general">
			<div class="ranking">
				<div class="number light">
					#2				</div>
				<div class="name">
					Poke-Planet-Pixelmon				</div>
			</div>
			<br class="server-visible-small">
			<div class="connection">
				<div class="status online">
				  Online				</div>
				<div class="version light">
				  1.7.2				</div>
				<div class="ip">
				  play.pokeplanet.net				</div>
			</div>
			<div class="light country">
				<img src="" class="flag flag-us">
			</div>
					</div>
	</div>
 
<style>

div.server:hover {
  color: lightgray;
  cursor: pointer;
  cursor: hand;
}

div.server {
  width: 100%;
  height: auto;
  color: white;
  word-wrap: break-word;
  word-break: break-all;
  margin-bottom: 3px;
  overflow: hidden;
  font-size: 12px;
}

div.server div.keywords {
  height: 25px;
  background: #4F4E4E;
  overflow-x: auto;
  overflow-y: hidden;
  padding-right: 15px;
  font-size: 11px;
}

div.server div.keywords:hover {
  overflow: visible;
  text-align: center;
}
div.server div.keywords div.keyword {
  padding-left: 7px;
  padding-right: 7px;
  height: 25px;
  line-height: 25px;
  background: #4F4E4E;
  float: left;
}
div.server div.keywords div.keyword:nth-child(odd) {  background: #8A8A8A }

div.server div.info div.description,
div.server div.info div.banner,
div.server div.info div.data {
  display: block;
  margin: 0;
  height: 60px;
  float: left;
}

div.server div.info div.description {
  background: #D8D8D8;
  width: 50%;
  float: left;
  margin-right: -234px;
}

div.server div.info div.description div.favicon {
  padding-top: 10px;
  padding-left: 10px;
  padding-right: 10px;
  float: left;
  width: 50px;
}

div.server div.info div.description div.favicon img {  height: 40px }

div.server div.info div.description div.intro {
  color: gray;
  font-size: 9px;
  line-height: 60px;
  text-overflow: ellipsis;
  padding-left: 10px;
  padding-right: 10px;
  word-break: normal;
  margin-right: 234px;
  width: auto;
  margin-left: 50px;
  float: none;
}

div.server div.info div.description div.intro span {
  display: inline-block;
  vertical-align: middle;
  line-height: normal;
}

div.server div.info div.banner {  width: 468px }

div.server div.info div.data {
  background: #5F5F5F;
  width: calc(50% - 234px);
}

div.server div.info div.data>div {
  line-height: 30px;
  height: 30px;
  width: 50%;
  text-align: right;
  padding-right: 10px;
}

div.server div.info div.data div.light {  background: #8A8A8A }

div.server div.info div.data div.left {  float: left }

div.server div.info div.data div.right {  float: right }

div.server div.info div.data span.glyphicon {
  line-height: 30px;
  float: left;
  padding-left: 10px;
}

div.server div.info div.data > div.players,
div.server div.info div.data > div.uptime {
  padding: 0;
  text-align: center;
}

div.server div.info div.data > div.uptime div.slider.good {  background: #47BA7F }

div.server div.info div.data > div.uptime div.slider.warning {  background: #c4ac10 }

div.server div.info div.data > div.uptime div.slider.bad {  background: #d9534f }

/* IE doesn't support this, so we take a more oldschool approach
      div.server div.info div.data>div:nth-child(odd) {
          float: left;
      }
      
      div.server div.info div.data>div:nth-child(even) {
          float: right;
      }
   ================================================== */

div.server div.general {
  display: block;
  margin-top: 60px;
  height: 25px;
  background: #2980B9;
  line-height: 25px;
}

div.server.premium div.general {  background: #BD8E02 }

div.server div.general div {
  display: inline-block;
  float: left;
  padding-left: 10px;
  padding-right: 10px;
}

div.server div.general div.ranking {
  padding: 0;
  float: left;
  width: 50%;
  margin-right: -234px;
}

div.server div.general div.name {
  max-width: 188px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}

div.server div.general div.connection {
  padding: 0;
  width: 432px;
}

div.server div.general div.light {  background: #3392D0 }

div.server.premium div.general div.light {  background: #C2AA63 }

div.server div.general div.connection div.status {
  padding: 0;
  text-align: center;
  width: 70px;
}

div.server div.general div.connection div.status.online {  background: #47BA7F }

div.server div.general div.connection div.status.offline {  background: #d9534f }

div.server div.general div.country {
  padding-left: 10px;
  padding-right: 10px;
}

div.server div.general div.country img {  margin-top: -2px }

div.server div.general div.premium {
  width: 113px;
  background: #C2AA63;
  text-align: center;
  padding: 0;
  float: right;
}

div.server div.general div.premium span.glyphicon {
  line-height: 25px;
  float: left;
  padding-left: 10px;
}

</style>
  
@stop
@section('title')
  {{ SERVERLIST_NAME }} - {{ Lang::get('server.strings.tos') }}
@stop