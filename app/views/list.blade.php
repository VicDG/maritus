@extends('layout')
@section('description', Lang::get('meta.description.home'))
@section('keywords', 'minecraft, minecraft server list, minecraft servers, minecraft server info, minecraft serverlist, minecraft top list, minecraft toplist, best minecraft servers', 'best minecraft server', 'best minecraft server list')
@section('title', SERVERLIST_NAME . ' ' . SERVERLIST_GAME . ' Serverlist - Find or add your perfect ' . SERVERLIST_GAME . ' server')
@section('styles')
  <style>
   div#end {
     background: #252525;
     margin-top: -60px;
   }
   html {
    background: none;
   }
  </style>
@stop

@section('content')
  <div class="white col-md-10 col-md-offset-1" style="margin-top:87px; margin-bottom: 72px;">
    <h2 style="display:inline-block;">{{ Lang::get('content.list.intro.title') }}&nbsp;</h2><div class="g-plusone" data-size="small" data-annotation="none"></div>
    <p>{{ Lang::get('content.list.intro.text') }}</p>
  </div>
  <div class="clear"></div>
  
  <div class="clear" style="height: 20px;"></div>
         
  <div class="isiborder">
  <div id="premiumtop">
    <h1>{{ Lang::get('content.list.titles.premium') }}</h1>
  </div>
  
  <!-- Advertisment -->
  <div class="ad inline-ad" style="margin-bottom: -6px; margin-top: 0;">
    <div class="img-thumbnail">
      <div class="visible-lg visible-md">
        <!--Large-->
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <!-- Minequery Block -->
        <ins class="adsbygoogle"
             style="display:inline-block;width:728px;height:90px"
             data-ad-client="ca-pub-1446213534151100"
             data-ad-slot="4693701475"></ins>
        <script>
        (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
      </div>
      <div class="visible-sm visible-xs" style="text-align: center;">
        <!--Small-->
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <!-- Minequery Block Small -->
        <ins class="adsbygoogle"
             style="display:inline-block;width:320px;height:50px"
             data-ad-client="ca-pub-1446213534151100"
             data-ad-slot="4112755070"></ins>
        <script>
        (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
      </div>
    </div>
  </div>
  <!-- / Advertisement -->
  
   <div class="premiumbanner">
     <div class="col-md-12" style="margin-bottom: 10px;">
       <div class="col-md-8">
         <h2>{{ Lang::get('content.list.premium.title') }}</h2>
         <p>{{ Lang::get('content.list.premium.text') }}</p>
     	</div>
       <div class="col-md-4">
         <div class="button-getpremium">
           <a href="{{ route('premium') }}" class="button-addserver">Get Premium</a>
         </div>
       </div>
     </div>
     <div class="clear"></div>
   </div>
  <div id="premium">
  	<!-- Premiums -->
  	@include('ajax/premium')
  	<!-- / Premiums -->
  </div>
  
  <!-- Advertisment -->
  <div class="ad inline-ad" style="margin-bottom: 0;">
    <div class="img-thumbnail">
      <div class="visible-lg visible-md">
        <!--Large-->
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <!-- Minequery Block -->
        <ins class="adsbygoogle"
             style="display:inline-block;width:728px;height:90px"
             data-ad-client="ca-pub-1446213534151100"
             data-ad-slot="4693701475"></ins>
        <script>
        (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
      </div>
      <div class="visible-sm visible-xs" style="text-align: center;">
        <!--Small-->
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <!-- Minequery Block Small -->
        <ins class="adsbygoogle"
             style="display:inline-block;width:320px;height:50px"
             data-ad-client="ca-pub-1446213534151100"
             data-ad-slot="4112755070"></ins>
        <script>
        (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
      </div>
    </div>
  </div>
  <!-- / Advertisement -->
  <div id="votetop">
    <div id="filterbar">
      <div id="sortname" class="col-md-5">
        <select id="sort" class="multiselect">
          <option value="votes">{{ Lang::get('content.list.titles.votes') }}</option>
          <option value="players">{{ Lang::get('content.list.titles.players') }}</option>
          <option value="score">{{ Lang::get('content.list.titles.score') }}</option>
          <option value="uptime">{{ Lang::get('content.list.titles.uptime') }}</option>
          <option value="created">{{ Lang::get('content.list.titles.created') }}</option>
          <option value="updated">{{ Lang::get('content.list.titles.updated') }}</option>
        </select>
        
        <select id="direction" class="multiselect">
          <option value="desc">{{ Lang::get('content.list.sort.descending') }}</option>
          <option value="asc">{{ Lang::get('content.list.sort.ascending') }}</option>
        </select>
        <span data-toggle="tooltip" title="" data-original-title="This influences the order in which the servers are listed." class="hidden-xs selector glyphicon glyphicon-question-sign">
        </span>
      </div>
      
      <div class="col-md-7" id="filter">
        <?php $countries = (is_array(json_decode(Cache::get('server_countries')))) ? json_decode(Cache::get('server_countries')) : Array(); ?>
        <select id="countries" class="multiselect" multiple="multiple">
          @foreach ($countries as $country)
            <option value="{{ $country }}">{{ $country }}</option>
          @endforeach
        </select>
        <select id="population" class="multiselect" multiple="multiple">
          <option value="cozy">{{ Lang::get('content.list.sort.cozy') }}</option>
          <option value="big">{{ Lang::get('content.list.sort.big') }}</option>
          <option value="massive">{{ Lang::get('content.list.sort.massive') }}</option>
        </select>
        <select id="version" class="multiselect" multiple="multiple">
          <?php $versions = (is_array(json_decode(Cache::get('versions')))) ? json_decode(Cache::get('versions')) : Array(); ?>
          @foreach ($versions as $version)
            @if(!empty($version))
              <option value="{{ $version }}">{{ $version }}</option>
            @endif
          @endforeach
        </select>
        <select id="keywords" class="multiselect" multiple="multiple">
          <option value="any">Any Keywords</option>
          @foreach (Keyword::all() as $keyword)
            <option value="{{ $keyword->id }}">{{ $keyword->name }}</option>
          @endforeach
        </select>
        &nbsp;<span data-toggle="tooltip" title="" data-original-title="{{ Lang::get('content.list.sort.tooltip.filter') }}" class="hidden-xs selector glyphicon glyphicon-question-sign">
        </span>
        <a id="reset-filters" style="color:white;" class="btn btn-link"><span data-toggle="tooltip" title="" data-original-title="{{ Lang::get('content.list.sort.tooltip.reset') }}" class="reset-tooltip glyphicon glyphicon-repeat"></span></a>
      </div>
    </div>
    <noscript>
      <h1>{{ Lang::get('content.list.titles.votes') }}</h1>
    </noscript>
  </div>
  
  <div id="list">
    <!-- List -->
	@include('ajax/list')
	<!-- / List -->
  </div>
  
  <div  class="footer-list">
    <noscript>
      <div class="paginate">
        {{ $servers->links() }}
      </div>
    </noscript>
    <div id="h-preload">
      <img alt="Preload" src="/img/h-preload.gif">
    </div>
  </div>
  
  </div>
@stop

@section('scripts')
  <script src="https://apis.google.com/js/platform.js" async defer></script>
@stop

