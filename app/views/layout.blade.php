<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      @if (isset($_GET['page']))
          <meta name="robots" content="noindex">
      @endif
      <meta name="keywords" content="@yield('keywords')">
      <meta name="description" content="@yield('description', Lang::get('meta.description.default'))">
      <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
      <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
      <title>
         @yield('title', SERVERLIST_NAME)
      </title>
      <!--  Mobile Viewport Fix -->
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
      <!-- This is the traditional favicon.
         - size: 16x16 or 32x32
         - transparency is OK
         - see wikipedia for info on browser support: http://mky.be/favicon/ -->
      <link rel="shortcut icon" href="/favicon.ico">
      <link rel="icon" sizes="16x16 32x32 64x64" href="/favicon.ico">
      <link rel="icon" type="image/png" sizes="196x196" href="/favicon-196.png">
      <link rel="icon" type="image/png" sizes="160x160" href="/favicon-160.png">
      <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96.png">
      <link rel="icon" type="image/png" sizes="64x64" href="/favicon-64.png">
      <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32.png">
      <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16.png">
      <link rel="apple-touch-icon" sizes="152x152" href="/favicon-152.png">
      <link rel="apple-touch-icon" sizes="144x144" href="/favicon-144.png">
      <link rel="apple-touch-icon" sizes="120x120" href="/favicon-120.png">
      <link rel="apple-touch-icon" sizes="114x114" href="/favicon-114.png">
      <link rel="apple-touch-icon" sizes="76x76" href="/favicon-76.png">
      <link rel="apple-touch-icon" sizes="72x72" href="/favicon-72.png">
      <link rel="apple-touch-icon" href="/favicon-57.png">
      <meta name="msapplication-TileColor" content="#FFFFFF">
      <meta name="msapplication-TileImage" content="/favicon-144.png">
      <meta name="msapplication-config" content="/browserconfig.xml">
      
      <!-- CSS -->
      @if(app()->env == 'local')
      	<?= stylesheet_link_tag() ?>
      @else
      	<?= stylesheet_link_tag(app()->env .'/'. 'application') ?>
      @endif
      
      <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>
      @yield('styles')
      <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
      <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
      <![endif]-->
   </head>
   <body>
      <!-- Background -->
      <!-- <div class="pbg"></div> -->
      <!-- Container -->
      <div class="container">
         <!-- Accessible from views -->
         @yield('beforenav')
         <!-- / -->
         <!-- Navbar -->
         <nav class="navbar navbar-inverse navbar-default navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
               <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
               <span class="sr-only">{{ Lang::get('server.strings.toggle_navigation') }}</span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               </button>
               <a class="navbar-brand" href="/"></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
               <ul class="nav navbar-nav">
                  <li class="{{ (Request::is('/') ? ' active' : '') }}"><a href="/"><span class="glyphicon glyphicon-home"></span>&nbsp; Serverlist</a></li>
                  @if ( !(Auth::check() || Admin::inSession()) )
                  <li class="{{ (Request::is('server/add') ? ' active' : '') }}"><a href="{{{ URL::to('server/add') }}}"><span class="glyphicon glyphicon-plus"></span>&nbsp; {{ Lang::get('server.strings.add_server') }}</a></li>
                  @endif
                  <li class="{{ (Request::is('premium') ? ' active' : '') }} visible-lg visible-md"><a href="{{{ URL::to('premium') }}}"><span class="glyphicon glyphicon-certificate"></span>&nbsp; Premium</a></li>
                  <li class="loginhide {{ (Request::is('page/help') ? ' active' : '') }}"><a href="{{{ URL::to('page/help') }}}"><i class="fa fa-question-circle"></i>&nbsp; Help</a></li>
                  <li class="loginhide dropdown">
                     <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="fa fa-ellipsis-v"></i>&nbsp;&nbsp;More <span class="caret"></span>
                     </a>
                     <ul class="dropdown-menu">
                        <li><a href="https://github.com/Moeflon/minequery-api"><span class="fa fa-code"></span>&nbsp;API</a></li>
                        <li><a href="{{{ URL::to('page/tos') }}}"><span class="fa fa-quote-right"></span>&nbsp;Terms of Service</a></li>
                     </ul>
                  </li>
               </ul>
               <ul class="nav navbar-nav navbar-right">
                   <li class="loginhide visible-lg">
                       <a href="https://plus.google.com/118306902802721297066" rel="publisher" target="_blank">
                           <span class="fa fa-google"></span>&nbsp; +Minequery
                       </a>
                   </li>
                   <li class="loginhide visible-lg">
                       <a target="_blank" href="https://twitter.com/minequery">
                           <span class="fa fa-twitter"></span>&nbsp; @Minequery
                       </a>
                   </li>
                  @if (!Auth::check())
                  <li class="hidden-lg {{ (Request::is('login') ? 'active' : '') }}">
                     <a href="{{{ URL::to('login') }}}">Log In</a>
                  </li>
                  @endif
                  @if (Auth::check())
                  <li class="dropdown {{ ((isset($server) && !isset($servers) && is_object($server) && $server->id == Auth::user()->id) ? ' active' : '') }}">
                     <a class="dropdown-toggle" data-toggle="dropdown" href="{{ URL::route('server.view', array(Auth::user()->name) ) }}">{{ Lang::get('server.messages.login.current', array('name' => Auth::user()->name)) }} <span class="caret"></span>
                     </a>
                     <ul class="dropdown-menu">
                        <li>{{ link_to_route('server.view', Lang::get('server.strings.view'), array(Auth::user()->name)) }}</li>
                        <li>{{ link_to_route('server.edit', Lang::get('server.strings.edit'), array(Auth::user()->name)) }}</li>
                        <li>{{ link_to_route('premium', Lang::get('server.strings.premium'), array()) }}</li>
                        <li class="divider"></li>
                        <li>{{ link_to_route('server.delete', Lang::get('server.strings.delete'), array(Auth::user()->name)) }}</li>
                     </ul>
                  </li>
                  <li><a href="{{{ URL::to('logout') }}}">{{ Lang::get('server.strings.logout') }}</a></li>
                  @elseif (Admin::inSession())
                  <li class="dropdown">
                     <a class="dropdown-toggle" data-toggle="dropdown" href="#">{{ Lang::get('server.messages.login.current', array('name' => Admin::currentAdmin()->name)) }} <span class="caret"></span>
                     </a>
                     <ul class="dropdown-menu">
                        <li>
                        </li>
                     </ul>
                  </li>
                  <li><a href="{{{ URL::to('admin/logout') }}}">{{ Lang::get('server.strings.logout') }}</a></li>
                  @else
                  @if(!Request::is('login'))
                  <li class="visible-lg">
                     <a id="login" href="#">Log In</a>
                  </li>
                  @endif
                  @endif
                  <li>
                     <div style="width: 50px;"></div>
                  </li>
               </ul>
               {{ Form::open(array('route' => 'login', 'class' => 'navbar-form navbar-left hidden pull-right', 'id' => 'login_form')) }}
               <div class="form-group">
                  {{ Form::text('name', '', array('class' => 'form-control', 'placeholder' => 'Server Name')) }}
                  {{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'Password')) }}
               </div>
               {{ Form::submit('Log In', array('class' => 'btn btn-success')) }}
               {{ Form::close() }}
            </div>
            <!-- /.navbar-collapse -->
         </nav>
         <!-- ./ navbar -->
         <div class="clear" style="margin-bottom: 100px;"></div>
         @if(count($rotator))
         <div class="toprotator">
            <a href="{{ route('server.view', array($rotator->first()->server->name)) }}" target="_blank">
            @if (!empty($rotator->first()->server->banner))
            <img alt="{{ SERVERLIST_GAME }} Premium Banner Rotator" src="{{ $rotator->first()->server->banner }}" class="server-banner img-responsive">
            @else
            <img alt="{{ SERVERLIST_GAME }} Premium Banner Rotator" src="{{ route('banner.small', $rotator->first()->server->name) }}" class="server-banner img-responsive">
            @endif
            </a>
         </div>
         @endif
         <!-- Content -->
         @if(Session::get('danger'))
         <div class="alert alert-danger">{{Session::get('danger')}}</div>
         @endif
         @if(Session::get('warning'))
         <div class="alert alert-warning">{{Session::get('warning')}}</div>
         @endif
         @if(Session::get('info'))
         <div class="alert alert-info">{{Session::get('info')}}</div>
         @endif
         @if(Session::get('success'))
         <div class="alert alert-success">{{Session::get('success')}}</div>
         @endif
         @if(!Request::is('/'))
         <!-- Advertisment -->
         <div class="ad">
            <div class="img-thumbnail">
               <div class="visible-lg visible-md">
                  <!--Large-->
                  <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                  <!-- Minequery Block -->
                  <ins class="adsbygoogle"
                       style="display:inline-block;width:728px;height:90px"
                       data-ad-client="ca-pub-1446213534151100"
                       data-ad-slot="4693701475"></ins>
                  <script>
                  (adsbygoogle = window.adsbygoogle || []).push({});
                  </script>
               </div>
               <div class="visible-sm visible-xs" style="text-align: center;">
                  <!--Small-->
                  <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                  <!-- Minequery Block Small -->
                  <ins class="adsbygoogle"
                       style="display:inline-block;width:320px;height:50px"
                       data-ad-client="ca-pub-1446213534151100"
                       data-ad-slot="4112755070"></ins>
                  <script>
                  (adsbygoogle = window.adsbygoogle || []).push({});
                  </script>
               </div>
            </div>
         </div>
         <br >
         <!-- / Advertisment -->
         @endif
         @yield('content')
         <!-- ./ content -->
      </div>
      <!-- ./ container -->
      <div id="end">
      </div>
      <div id="end2">
         <div class="container">
            <div class="col-md-8 clearfix" id="footer-menu">
               <ul>
                  <li><a href="{{{ URL::to('/') }}}">Serverlist</a></li>
                  <li><a href="{{{ URL::to('server/add') }}}">Add Server</a></li>
                  <li><a href="mailto:support@minequery.net">Report a bug</a></li>
                  <li><a href="{{{ URL::to('page/tos') }}}">Terms of Service</a></li>
               </ul>
               @if(count($rotator))
               <br>
               <div class="bottomrotator img-thumbnail">
                  <a href="{{ route('server.view', array($rotator->last()->server->name)) }}" target="_blank">
                  @if (!empty($rotator->last()->server->banner))
                  <img alt="{{ SERVERLIST_GAME }} Premium Banner Rotator" src="{{ $rotator->last()->server->banner }}" class="server-banner img-responsive">
                  @else
                  <img alt="{{ SERVERLIST_GAME }} Premium Banner Rotator" src="{{ route('banner.small', $rotator->last()->server->name) }}" class="server-banner img-responsive">
                  @endif
                  </a>
               </div>
               @endif
            </div>
            <div class="col-md-4">
            	<br class="visible-xs visible-sm" >
            	@if(app()->env == 'pixelmonlist')
            		<a href="http://www.pixelmonmod.com/?ref=pixelmonlist" target="_blank"><img src="/img/pixelmonmod.png" width="250" height="72" border="0"></a>
            	@else
               		<a href="#" target="_blank"><img alt="Minequery Network" src="/img/minequery_banner.png" width="250" height="72"></a>
               	@endif
            </div>
            <div class="col-md-12">
               @if(!Request::is('/'))
               <!-- Advertisment -->
               <br>
               <div class="ad">
                  <div class="bottomad img-thumbnail">
                     <div class="visible-lg visible-md">
                        <!--Large-->
                        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                        <!-- Minequery Block -->
                        <ins class="adsbygoogle"
                             style="display:inline-block;width:728px;height:90px"
                             data-ad-client="ca-pub-1446213534151100"
                             data-ad-slot="4693701475"></ins>
                        <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>
                     </div>
                     <div class="visible-sm visible-xs" style="text-align: center;">
                        <!--Small-->
                        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                        <!-- Minequery Block Small -->
                        <ins class="adsbygoogle"
                             style="display:inline-block;width:320px;height:50px"
                             data-ad-client="ca-pub-1446213534151100"
                             data-ad-slot="4112755070"></ins>
                        <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>
                     </div>
                  </div>
               </div>
               <!-- / Advertisment -->
               @endif
               <br>
               <p>© Minequery, 2014. All rights reserved</p>
               <p>Minecraft is a trademark and copyright of Mojang AB. {{ SERVERLIST_NAME }} is not affiliated with Mojang AB.</p>
            </div>
         </div>
      </div>
      <!-- Javascripts -->
      <?php
         try {
        	$controller = strtolower(Active::getController()); 
         } 
         catch (Exception $e) {
         	$controller = 'main';
         }
         ?>
      @if(in_array($controller, array('list', 'server')))
      <?= javascript_include_tag($controller . '/' . 'application') ?>
      @else
      <?= javascript_include_tag() ?>
      @endif
      @yield('scripts') 
      <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-40098278-1', 'auto');
        ga('send', 'pageview');

      </script>
   </body>
</html>