@extends('layout')

@section('title')
  {{ $server->name }} - {{ Lang::get('server.strings.edit') }}
@stop

@section('content')
  @include('forms/server')
@stop