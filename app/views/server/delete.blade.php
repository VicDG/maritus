@extends('layout')

@section('title')
  {{ $server->name }} - {{ Lang::get('server.strings.delete') }}
@stop

@section('content')
<div class="well">
  <h2>{{ Lang::get('server.strings.delete') }}</h2>
  <hr/>
  <p>
    {{ Lang::get('server.messages.delete.confirm') }}
  </p>
  <hr/>
  @if ($wrong === true)
    <div class="alert alert-danger">{{ Lang::get('server.messages.delete.incorrect') }}t</div>
    <hr/>
  @endif
  {{ Form::open(array('route' => array('server.remove', $server->name), 'class' => 'form-horizontal', 'role' => 'form')) }}
    <div class="form-group {{ ($wrong === true) ? 'has-error' : '' }}">
      {{ Form::label('password', Lang::get('server.strings.password'), array('class' => 'control-label col-md-4')) }}
      <div class="col-md-4">
        {{ Form::password('password', array('class' => 'form-control')) }}
      </div>
    </div>
    <hr/>
     <div class="form-group">
       <div class="col-md-offset-7 col-md-4">
          {{ Form::submit(Lang::get('server.strings.delete'), array('class' => 'btn btn-primary btn-lg btn-success')) }}
       </div>
    </div>
  {{ Form::close() }}
</div>
@stop