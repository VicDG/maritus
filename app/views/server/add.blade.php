@extends('layout')
@section('description', Lang::get('meta.description.add'))

@section('title')
  {{ SERVERLIST_NAME }} - {{ Lang::get('server.strings.add_server') }}
@stop

@section('content')
  @include('forms/server')
@stop