@section('description', Lang::get('meta.description.vote'))
<h2>{{ Lang::get('server.strings.votes.recent') }}</h2>
<hr/>
<div class="well">
@if ($server->votes()->count() > 0)
    @foreach ($server->votes()->where('player', '!=', '')->orderBy('created_at', 'desc')->take(36)->distinct()->get(array('player')) as $vote)
      <img class="avatar" data-toggle="tooltip" data-placement="top" title="{{ $vote->player }}" alt="{{ $vote->player }}" width="32px" src="{{ Avatar::url($vote->player, 64) }}"></img>
    @endforeach
@else
  {{ Lang::get('server.strings.votes.none') }}
@endif
</div>
@if ($server->canVote())
  <h2>{{ Lang::get('server.strings.vote') }}  
  @if (!empty($server->votifier_ip) && !empty($server->votifier_port) && !empty($server->votifier_key))
    <small>{{ Lang::get('server.strings.votifier.uses') }}</small>
  @endif</h2>
  <hr/>
  {{ Form::open(array('route' => array('vote.create', $server->name), 'class' => 'form-horizontal', 'role' => 'form')) }}
    <div class="form-group">
      {{ Form::label('player', Lang::get('server.strings.minecraft_username'), array('class' => 'control-label col-md-4')) }}
      <div class="col-md-4">
        {{ Form::text('player', '', array('class' => 'form-control')) }}
      </div>
    </div>
    {{ Form::hidden('tlpm', md5(SERVERLIST_NAME.time()).','.time()) }}
    <hr/>
    <div class="col-md-8 col-md-offset-2">
      <div class="captcha">
        <span>
          {{ Lang::get('server.messages.vote.slide') }}
        </span>
      </div>
    </div>
    <div class="form-group">
      <div class="col-md-offset-7 col-md-4">
        {{ Form::submit(Lang::get('server.strings.vote'), array('class' => 'btn btn-warning btn-lg btn-success hidden')) }}
      </div>
    </div>
  {{ Form::close() }}
@else
  <h2>Vote</h2>
  <hr/>
  <div class="alert alert-danger">{{ Lang::get('server.messages.vote.already') }}</div>
@endif

<!-- Vote Button -->
<hr/>
<div class="col-md-4">
  <iframe src="{{ URL::route('api.button', array($server->name)) }}" style="border:0;overflow:hidden;" height="45" width="240"></iframe>
</div>
<div class="form-group col-md-8">
  <input type="text" value='<script src="{{ URL::route('api.buttonjs', array($server->name)) }}"></script>' class="form-control to-copy">
  <span class="help-block">{{ Lang::get('server.strings.embed.vote') }}</span>
</div>
<div class="clear"></div>