@section('description', Lang::get('meta.description.players', array('name' => $server->name)))
<h2>{{ Lang::get('server.strings.players') }}</h2>
<hr/>
@if ($server->cache->lastPlayers()->first()->names && count(json_decode($server->cache->lastPlayers()->first()->names)) > 0)
  <div class="well">
  @foreach (json_decode($server->cache->lastPlayers()->first()->names) as $player)
    <img class="avatar" width="32px" data-toggle="tooltip" data-placement="top" title="{{ $player }}" alt="{{ $player }}" src="{{ Avatar::url($player, 64) }}"></img>
  @endforeach
  </div>
  <div class="form-group">
    <input type="text" value='<script src="{{ URL::route('api.playerlist', array($server->name)) }}"></script>' class="form-control to-copy">
    <span class="help-block">You can use this code to display the avatars of online players on your own webstie</span>
  </div>
@elseif (!$server->isOnline())
  {{ Lang::get('server.strings.players_tab.offline') }}
@elseif ($server->cache->lastPlayers->first()->count == 0)
  {{ Lang::get('server.strings.players_tab.none') }}
@else
  {{ Lang::get('server.strings.players_tab.noaccess') }}
@endif
<hr/>

<h2>{{ $server->name }}  {{ Lang::get('server.strings.player_history') }}</h2>
<hr/>
<iframe src="{{ URL::route('api.chart', array($server->name, 200)) }}" style="border:0;overflow:hidden;width:100%;height:220px;"></iframe>
<div class="form-group">
  <input type="text" value='<script src="{{ URL::route('api.playersjs', array($server->name)) }}"></script>' class="form-control to-copy">
  <span class="help-block">{{ Lang::get('server.strings.embed.playerlist') }}</span>
</div>

