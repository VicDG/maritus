@section('description', Lang::get('meta.description.login'))
<h2>{{ Lang::get('server.strings.login') }}</h2>
<hr/>
{{ Form::open(array('route' => 'login', 'class' => 'form-horizontal', 'role' => 'form')) }}
    {{ Form::hidden('name', $server->name, array('class' => 'form-control')) }}
  <div class="form-group">
    {{ Form::label('password', Lang::get('server.strings.password'), array('class' => 'control-label col-md-4')) }}
    <div class="col-md-4">
      {{ Form::password('password', array('class' => 'form-control')) }}
    </div>
  </div>
  <hr/>
  <div class="form-group">
    <div class="col-md-offset-3 col-md-4">
      <div class="checkbox">
        <label>
          {{ Form::checkbox('remember') }} Remember Me
        </label><br/>
        <small>{{ Lang::get('server.messages.login.create', array('link' => link_to_route('server.add', Lang::get('server.strings.register')))) }}</small>
        <hr class="visible-xs visible-sm" />
      </div>
    </div>
    <div class="col-md-4">
      {{ Form::submit(Lang::get('server.strings.login'), array('class' => 'btn btn-primary btn-lg btn-success')) }}
    </div>
  </div>
{{ Form::close() }}