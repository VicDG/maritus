@section('description', Lang::get('meta.description.banners'))
@if (!empty($server->banner))
  <h2>{{ Lang::get('server.strings.banner') }}</h2>
  <hr/>
  <div class="banner-container">
    <div class="img-thumbnail">
      <img alt="{{ $server->name }} Minecraft Server Banner" src="{{ $server->banner }}" class="server-banner img-responsive"></img>
    </div>
  </div>
  <hr/>
@endif
<h2>{{ Lang::get('server.strings.generated_banners') }}</h2>
<hr/>
<div class="banner-container">
  <div class="img-thumbnail">
    <img alt="{{ $server->name }} Dynamic Minecraft Server Banner" src="{{ route('banner.small', $server->name) }}" class="server-banner img-responsive"></img>
    <input type="text" value='<img src="{{ route('banner.small', $server->name) }}" width="486px" height="60px"></img>' class="banner-code form-control to-copy">
  </div>
</div>
<hr/>
<div class="banner-container">
  <div class="img-thumbnail">
    <img alt="{{ $server->name }} Dynamic Minecraft Server Banner" src="{{ route('banner.big', $server->name) }}" class="server-banner img-responsive"></img>
    <input type="text" value='<img src="{{ route('banner.big', $server->name) }}" width="500px" height="80px"></img>' class="banner-code form-control to-copy">
  </div>
</div>