@section('description', Lang::get('meta.description.reviews', array('name' => $server->name)))
<h2>{{ Lang::get('server.strings.reviews') }}</h2>
<hr>
@if (Session::get('wrong') && Session::get('wrong') == true)
  <div class="alert alert-danger">{{ Lang::get('server.messages.review.missing') }}</div>
  <hr>
@endif

{{ Form::open(array('url' => URL::route('review.post', array($server->name)), 'style' => 'min-height: 310px;')) }}
  <div class="col-sm-2">
    <img alt="Minecraft Player" class="avatar" id="review-player" data-placement="top" width="80" src="{{ Avatar::url('Steve', 96) }}">
    <hr class="visible-xs" >
  </div>
  
  <div class="col-sm-10">
    <div class="form-group {{ (Session::get('wrong') && Session::get('wrong') == true)  ? 'has-error' : '' }}">
      <div>
        {{ Form::text('player', null, array('id' => 'review-player-name', 'class' => 'form-control', 'placeholder' => Lang::get('server.strings.minecraft_username'))) }}
        {{ $errors->first('player', '<br><div class="alert alert-danger">:message</div>') }}
      </div>
    </div>
  
    <div class="form-group">
      {{ Form::textarea('review', null, array('id' => 'editor', 'class' => 'form-control review-text')) }}
      {{ $errors->first('review', '<br><div class="alert alert-danger">:message</div>') }}
      <hr>
      <div class="captcha"><span>{{ Lang::get('content.review.slide') }}</span></div>
    </div>
  </div>

  <div class="form-group">
    <div class="col-md-12">
       {{ Form::submit(Lang::get('server.strings.submit'), array('class' => 'btn btn-primary btn-lg btn-success hidden')) }}
    </div>
  </div>
  {{ Form::hidden('tlpm', md5(SERVERLIST_NAME.time()).','.time()) }}
{{ Form::close() }}

<?php $reviews = $server->reviews()->orderBy('created_at', 'desc')->paginate(10); ?>
@if (!$reviews->isEmpty())
  @foreach ($reviews as $review)
  <hr class="col-md-11" >
    <div class="review col-md-12">
      <div class="col-xs-2">
        <div class="pull-right">
          <img alt="{{ $review->player }}" class="avatar" data-placement="top" width="50" src="{{ Avatar::url($review->player, 96) }}">
        </div>
      </div>
      <div class="col-xs-10">
        <p>
          {{ $bbcode->parse($review->review) }}
        </p>
        <hr>
        <p><small>{{ Lang::get('server.strings.review_author', array('player' => '<strong>'.$review->player.'</strong>', 'date' => $review->created_at->toDayDateTimeString())) }}</small></p>
      </div>
    </div>
  @endforeach
  <div class="paginate">
    {{ $reviews->links() }}
  </div>
  <div style="margin-bottom: 20px;" class="clear"></div>
@endif

<!-- Form init -->
<script>
  var wbbOpt = {
    lang : 'en',
    buttons : 'bold,italic,underline,|,bullist,numlist,|,removeFormat'
  }
  $('#editor').wysibb(wbbOpt);
  
  $('#review-player-name').focusout(function() {
    if($('#review-player-name').val() != '') {
      $('#review-player').attr('src', $('#review-player').attr('src').replace("Steve", $('#review-player-name').val()) );
    }
  });
</script>
