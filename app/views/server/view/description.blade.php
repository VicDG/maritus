<h2>{{ Lang::get('server.strings.information', array('serverlist_game' => SERVERLIST_GAME)) }}</h2>
<hr>

<!-- Badge -->
<div class="col-md-2">
  <img alt="{{ $server->name }} ' . SERVERLIST_GAME . ' Server Status Badge" src="{{ URL::route('api.badge', array($server->name)) }}" id="onlinebutton" height="36">
</div>
<div class="form-group col-md-10">
  <input type="text" value='<img src="{{ URL::route('api.badge', array($server->name)) }}" height="35">' class="form-control to-copy">
  <span class="help-block">{{ Lang::get('server.strings.embed.badge') }}</span>
</div>

<!-- Information table -->
<div class="table-responsive">
  <table class="table table-striped">
    <tr>
      <th>
        {{ Lang::get('server.strings.name') }}
      </th>
      <td>
        {{ $server->name }}
      </td>
    </tr>
    @if($server->keywords->first())
      <tr>
        <th>
          {{ Lang::get('server.strings.keywords') }}
        </th>
        <td>
          @foreach ($server->keywords as $keyword)
            <code>{{ $keyword->name }}</code>
          @endforeach
        </td>
      </tr>
    @endif
    @if(!empty($server->website))
      <tr>
        <th>
          {{ Lang::get('server.strings.website') }}
        </th>
        <td>
          <a target="_blank" itemprop="url" href="{{ 'http://' . str_replace('http://', '', $server->website) }}">{{ 'http://' . str_replace('http://', '', $server->website) }}</a>
        </td>
      </tr>
    @endif
    <tr>
      <th>Spread the &nbsp;<span class="fa fa-heart"></span></th>
      <td>
        <a href="https://twitter.com/share" data-via="{{ SERVERLIST_NAME }}" data-text="I just found this awesome {{ SERVERLIST_GAME }} server called {{ $server->name }}" data-related="{{ SERVERLIST_NAME }}" class="twitter-share-button" data-lang="en" data-size="small"></a>
        <br>
        <iframe src="//www.facebook.com/plugins/like.php?href={{ Request::url() }}" style="border:none; overflow:hidden; height:80px;" allowTransparency="true"></iframe>
      </td>
    </tr>
  </table>
</div>

<!-- Actual Description -->
@if(!empty($server->description))
  <h2>{{ Lang::get('server.strings.description') }}</h2>
  <hr>
  
  @if (!empty($server->banner))
    <!-- Banner -->
    <div class="banner-container">
      <div class="img-thumbnail">
        <img src="{{ $server->banner }}" class="server-banner img-responsive">
      </div>
    </div>
    <hr>
  @endif
  
  <div itemprop="description" id="description">
    {{ $bbcode->parse($server->description) }}
  </div>
@endif
