@extends('layout')
<?php $metakeywords = array(); ?>
  @if($server->keywords()->get()->first())
    @foreach ($server->keywords as $keyword)
      <?php array_push($metakeywords, $keyword->name); ?>
    @endforeach
  @endif
<?php $metakeywords = $metakeywords + (array)explode(' ',$server->cache->version); ?>
@section('keywords', implode(',', $metakeywords))
@section('description',  $server->name . ' server overview. Get the current status of the ' . $server->name . ' ' . SERVERLIST_GAME . ' server. Server owners can embed the ' . SERVERLIST_GAME . ' playerlist, graph, status badge, dynamic banner and vote button for free.' )

@section('title')
  {{ $server->name }} {{ SERVERLIST_GAME }} Server - {{ SERVERLIST_NAME }} 
@stop

@section('styles')
  <style>
    div#end {
      background: #252525;
      margin-top: -60px;
    }
    html {
      background: none;
    }
  </style>
@stop

@section('content')
<?php use Carbon\Carbon; ?>
<div itemscope itemtype="http://schema.org/Thing">
  <!-- Sidebar -->
    <div class="sidebar col-md-3">
        
      <div class="server-side {{ (@$server->premium->exists && @$server->premium->paid >=5) ? 'premium' : '' }}">
          <div class="head">
              @if (@$server->premium->exists)
              <div class="is-premium light">
                  <span class="glyphicon glyphicon-certificate"></span>
              </div>
              @endif
              <div itemprop="name" class="name">
                  <p>{{ $server->name }}</p>
              </div>
          </div>
          <div class="body">
              <div class="uptime">
                  <span class="fa fa-arrow-circle-up"></span>
                  <div style="width: {{ ($server->uptime < 60) ? '100' : $server->uptime }}%;" class="slider {{ $server->statusBar() }}">
                      {{ $server->uptime }}%
                  </div>
              </div>
              <div class="players">
                  <span class="fa fa-users"></span>
                  {{ ($server->playermax) ? $server->playercount . ' / ' . $server->playermax : '<i>N/A</i>' }}
              </div>
              <div class="score">
                  <span class="glyphicon glyphicon-fire"></span>
                  {{ $server->roundScore() }}
              </div>
              <div class="votes">
                  <span class="glyphicon glyphicon-check"></span>
                  {{$server->votes()->count()}}
                  {{ link_to_route('server.view', Lang::get('server.strings.vote'), array($server->name, 'vote'), array('class' => 'btn btn-xs btn-warning pull-right', 'id' => 'vote-button')) }}
              </div>
              <div class="version">
                  <span class="glyphicon glyphicon-wrench"></span>
                  {{ ($server->cache->version) ? $server->cache->version : '<i>N/A</i>' }} 
              </div>
              <div class="since">
                  <span class="glyphicon glyphicon-calendar"></span>
                  {{ Carbon::now()->subSeconds($server->created_at->diffInSeconds(Carbon::now()))->diffForHumans() }}  
              </div>
              <div class="refresh">
                  <span class="fa fa-repeat"></span>
                  {{ ($server->cache->updated_at) ? Carbon::now()->subSeconds($server->cache->updated_at->diffInSeconds(Carbon::now()))->diffForHumans() : 'Not yet polled' }}
              </div>
          </div>
          <div class="footer">
              <div class="social light">
                  <div class="g-plusone" data-size="small" data-annotation="none"></div>
              </div>
              <div class="light country">
                  <img alt="Country Flag" src="blank.gif" class="flag flag-{{ strtolower($server->country) }}">
              </div>
          </div>
      </div>
    </div>
    <!-- ./ sidebar -->
    
    <!-- Main Content -->
    <div class="main col-md-9">
      <!-- Connection Info -->
      <br class="visible-xs visible-sm" >
      
      <div class="server-top {{ (@$server->premium->exists && @$server->premium->paid >=5) ? 'premium' : '' }}">
        <div class="top">
            <div class="favicon">
                <img itemprop="image" alt="{{ $server->name }} {{ SERVERLIST_GAME }} Favicon" src="{{ route('favicon', $server->name) }}" >
            </div>
            <div class="ip">
                {{ $server->ip }}{{ (!empty($server->port) && $server->port != '25565') ? ':' . $server->port : '' }}
            </div>
        </div>
        <div class="bottom">
            <div class="status {{ ((bool)@$server->cache->status) ? 'online' : 'offline' }}">
                {{ ((bool)@$server->cache->status) ? 'Online' : 'Offline' }}
            </div>
            @if ($server->hasPermission())
            <div class="dropdown button">
              <button type="button" class="btn btn-info dropdown-toggle btn-xs" data-toggle="dropdown">
                {{ Lang::get('server.strings.manage') }} <span class="caret"></span>
              </button>
              <ul id="server-manage" class="dropdown-menu" role="menu">
                <li>{{ link_to_route('server.edit', Lang::get('server.strings.edit'), array($server->name)) }}</li>
                <li>{{ link_to_route('premium', Lang::get('server.strings.premium'), array()) }}</li>
                <li class="divider"></li>
                <li>{{ link_to_route('server.delete', Lang::get('server.strings.delete'), array($server->name)) }}</li>
                @if (Admin::inSession())
                  <li class="divider"></li>
                  <li>{{ link_to_route('admin.premium', 'Admin Premium', array($server->name)) }}</li>
                @endif
              </ul>
            </div>
            @endif
            <div class="motd">
                {{ ($server->cache->motd) ? $server->cache->motd : '<i>N/A</i>' }}
            </div>
        </div>
      </div>
      <div class="clear"></div> 

      <!-- Tabs -->
      <ul id="view-nav" class="nav nav-tabs nav-justified">
        @foreach ($tabs as $key => $t)
          @if($t == 'reviews')
            <li {{ ( ($key == 0 && !$tab) || (strtolower($t) == strtolower($tab)) ) ? 'class="active"' : '' }}><a href="{{ URL::route('server.view', array($server->name, $t)) }}" id="{{ $t }}">{{ ucfirst(Lang::get('server.strings.'.$t)) }}</a></li>
          @else
            <li {{ ( ($key == 0 && !$tab) || (strtolower($t) == strtolower($tab)) ) ? 'class="active"' : '' }}><a href="#" class="{{ $t }}" rel="next">{{ ucfirst(Lang::get('server.strings.'.$t)) }}</a></li>
          @endif
        @endforeach
      </ul>
      
      <!-- Loading -->
      <div id="preload" class="preload-container server-preload hidden">
        <img alt="Preloader" class="preload centered" src="{{ URL::to('img/preload.gif') }}">
      </div>
      
      <!-- Actual content -->
      <div id="view-content">
        @if ($tab)
          @include('server/view/' . $tab)
        @else
          @include('server/view/' . $tabs[0])
        @endif
      </div>
    </div>
</div>
@stop

@section('scripts')
  <script src="https://apis.google.com/js/platform.js" async defer></script>
  <script>
    $('.avatar').tooltip();
    var ajaxbase = '{{ route('server.view', array($server->name)) }}';
    var title = $(document).attr('title')
    var initialTab = '{{ ($tab) ? $tab : "description"}}';
    var tabName = '{{ ($tab) ? ucfirst($tab) : "Description" }}';
    History.replaceState({ tab: initialTab }, title + ' - ' + tabName, document.location.href);
  </script>
@stop
