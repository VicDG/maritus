@extends('layout')
@section('description', Lang::get('meta.description.reset'))
@section('title')
  {{ SERVERLIST_NAME }} - {{ Lang::get('server.strings.password_reset') }}
@stop
@section('content')
<div class="well">
  <h2>{{ Lang::get('server.strings.reset_password') }}</h2>
  <hr/>
  {{ Form::model($server, array('route' => array('reset.set', $id, $token), 'class' => 'form-horizontal', 'role' => 'form')) }}
    <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
      {{ Form::label('password', Lang::get('server.strings.password'), array('class' => 'control-label col-md-4')) }}
      <div class="col-md-3">
        {{ Form::password('password', array('class' => 'form-control')) }}
      </div>
    </div>
    <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
      {{ Form::label('password_confirmation', Lang::get('server.strings.retype_password'), array('class' => 'control-label col-md-4')) }}
      <div class="col-md-3">
        {{ Form::password('password_confirmation', array('class' => 'form-control')) }}
        {{ $errors->first('password', '<br/><div class="alert alert-danger">:message</div>') }}
      </div>
    </div>  
    <hr/>
    <div class="form-group">
      <div class="col-md-offset-7">
        <div class="col-md-4">
          {{ Form::submit(Lang::get('server.strings.reset'), array('class' => 'btn btn-primary btn-lg btn-success')) }}
        </div>
      </div>
    </div>
  {{ Form::close() }}
</div>
@stop