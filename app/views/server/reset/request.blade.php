@extends('layout')
@section('description', Lang::get('meta.description.reset'))
@section('title')
   {{ SERVERLIST_NAME }} - {{ Lang::get('server.strings.password_reset') }}
@stop
@section('content')
<div class="well">
  <h2>{{ Lang::get('server.strings.reset_password') }}</h2>
  <hr/>
  @if ($wrong === true)
    <div class="alert alert-danger">{{ Lang::get('server.messages.reset.incorrect') }}</div>
    <hr/>
  @endif
  {{ Form::open(array('route' => 'reset.get', 'class' => 'form-horizontal', 'role' => 'form')) }}
    <div class="form-group {{ ($wrong === true) ? 'has-error' : '' }}">
      {{ Form::label('email', Lang::get('server.strings.email'), array('class' => 'control-label col-md-4')) }}
      <div class="col-md-4">
        {{ Form::text('email', '', array('class' => 'form-control')) }}
      </div>
    </div>
    <div class="form-group {{ ($wrong === true) ? 'has-error' : '' }}s">
      {{ Form::label('name', Lang::get('server.strings.server_name'), array('class' => 'control-label col-md-4')) }}
      <div class="col-md-4">
        {{ Form::text('name', '', array('class' => 'form-control')) }}
      </div>
    </div>
    <center><small>{{ Lang::get('content.reset.both') }}</small></center>
    <hr/>
    <div class="form-group">
      <div class="col-md-offset-7">
        <div class="col-md-4">
          {{ Form::submit(Lang::get('server.strings.reset'), array('class' => 'btn btn-primary btn-lg btn-success')) }}
        </div>
      </div>
    </div>
  {{ Form::close() }}
</div>
@stop
