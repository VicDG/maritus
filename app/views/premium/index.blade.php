@extends('layout')
@section('description', Lang::get('meta.description.premium'))
@section('title')
  {{ SERVERLIST_NAME }} - {{ Lang::get('server.strings.premium') }}
@stop
@section('content')
<div class="well">
  <h2>{{ Lang::get('server.strings.premium') }}</h2>
  <hr/>
  <div class="col-md-8">
    {{ Lang::get('page.premium', array('serverlist_game' => SERVERLIST_GAME)) }}
  </div>
  <div id="pay" class="col-md-4">
    <h4 style="margin-top: 0;">{{ Lang::get('content.premium.titles.wantpay') }}</h4>
    <p>{{ Lang::get('content.premium.text.approximation') }}</p>
    {{ Form::open(array('route' => 'premium', 'class' => 'form-horizontal', 'role' => 'form')) }}
      <div class="input-group">
        <span class="input-group-addon">€</span>
        <input type="text" id="amount" name="amount" class="form-control">
        <span class="input-group-addon">.00</span>
        <span id="views" class="input-group-addon">0 {{ Lang::get('content.premium.strings.views') }}</span>
      </div>
      <hr/>
      <div class="form-group">
        <div class="col-md-8">
          <p>{{ Lang::get('content.premium.text.length') }}</p>
        </div>
        <div class="col-md-2">
          {{ Form::submit(Lang::get('content.premium.strings.buy'), array('class' => 'btn btn-primary btn-lg btn-success')) }}
        </div>
      </div>
      <p>{{ Lang::get('content.premium.text.thanks') }}.</p>
    {{ Form::close() }}
  </div>
  <div class="clear"></div>
</div>
@stop
@section('styles')
  <style>
    .is_stuck {
      margin-top: 20px;
    }
  </style>
@stop
@section('scripts')
  <script>
    function isNatural(s) {
      var n = parseInt(s, 10);
      return n >= 0 && n.toString() === s;
    }
    var timeoutReference;
    $(document).ready(function() {
      $('#pay').stick_in_parent();
      $('input#amount').keypress(function() {
        var _this = $(this); // copy of this object for further usage
        
        if (timeoutReference) clearTimeout(timeoutReference);
        timeoutReference = setTimeout(function() {
          if(isNatural(_this.val()) && parseInt(_this.val()) !== 0 && parseInt(_this.val()) <= 50) {
            amount = _this.val();
          }
          else {
            _this.val('');
            $('#views').html('0 views');
            return false;
          }
          $.get('/premium/estimate/' + amount, function(data) {
             $('#views').html(data + ' views');
          });
        }, 300);
      });
    }); 
  </script>
@stop