@extends('layout')
@section('title')
  {{ SERVERLIST_NAME }} - {{ $code }}
@stop
<?php
  if(!isset($rotator)) {
    $rotator = Premium::getPremiums(2, true);
  }
?>
@section('styles')
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>
  <style>
    html { 
      background: url('/img/error.png') no-repeat center center fixed; 
      -webkit-background-size: cover;
      -moz-background-size: cover;
      -o-background-size: cover;
      background-size: cover;
    }
    #containter {
      position: absolute;
      text-align: center;
      bottom: 50;
      left: 0;
      right: 0;
      margin: 0 auto;
      font-family: "Open Sans";
      word-break: break-all;
      padding: 20px;
    }
    .code {
      color: #fff9d6;
      font-weight:bold;
      font-size: 10em;
      text-shadow:0px 0px 0 rgb(226,226,226),1px 1px 0 rgb(207,207,207),2px 2px 0 rgb(188,188,188),3px 3px 0 rgb(169,169,169),4px 4px 0 rgb(150,150,150),5px 5px 0 rgb(131,131,131), 6px 6px 0 rgb(112,112,112),7px 7px 6px rgba(0,0,0,0.45),7px 7px 1px rgba(0,0,0,0.5),0px 0px 6px rgba(0,0,0,.2);
    }
    #end, #end2 {
      display: none;
    }
    body {
      background: none;
    } 
    .message {
      margin-top: -30px;
      color: #d1d1d1;
      font-size: 2em;
    }
  </style>
@stop
<div id="containter">
  <div class="code">{{ $code }}</div>
  <div class="message">{{ $message }}</div>
</div>