@extends('layout')
@section('description', Lang::get('meta.description.login'))

@section('title')
  {{ Lang::get('server.strings.login') }} - {{ SERVERLIST_NAME }}
@stop

@section('content')
<div class="well">
  <h2>{{ Lang::get('server.strings.login') }}</h2>
  <hr/>
  @if ($wrong === true)
    <div class="alert alert-danger">{{ Lang::get('server.messages.login.incorrect') }}</div>
    <hr/>
  @endif
  {{ Form::open(array('route' => 'login', 'class' => 'form-horizontal', 'role' => 'form')) }}
    <div class="form-group {{ ($wrong === true) ? 'has-error' : '' }}">
      {{ Form::label('name', Lang::get('server.strings.server_name'), array('class' => 'control-label col-md-4')) }}
      <div class="col-md-4">
        {{ Form::text('name', '', array('class' => 'form-control')) }}
      </div>
    </div>
    <div class="form-group {{ ($wrong === true) ? 'has-error' : '' }}">
      {{ Form::label('password', Lang::get('server.strings.password'), array('class' => 'control-label col-md-4')) }}
      <div class="col-md-4">
        {{ Form::password('password', array('class' => 'form-control')) }}
      </div>
    </div>
    <hr/>
     <div class="form-group">
       <div class="col-md-offset-3 col-md-4">
         <div class="checkbox">
           <label>
             {{ Form::checkbox('remember') }} {{ Lang::get('server.strings.remember_me') }}
           </label>
         </div>
         <small>{{ Lang::get('server.messages.login.create', array('link' => link_to_route('server.add', Lang::get('server.strings.register')))) }}</small><br/>
         <small>{{ Lang::get('server.messages.reset.forgot_password', array('link' => link_to('reset/request', Lang::get('server.strings.reset')))) }}</small>
         <hr class="visible-xs visible-sm" />
       </div>
       <div class="col-md-4">
          {{ Form::submit(Lang::get('server.strings.login'), array('class' => 'btn btn-primary btn-lg btn-success')) }}
       </div>
    </div>
  {{ Form::close() }}
</div>
@stop