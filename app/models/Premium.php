<?php
use LaravelBook\Ardent\Ardent;
use Carbon\Carbon;
class Premium extends Ardent
{
    protected $table = 'premiums';
    private $ydDisplays;
    private function ydDisplays()
    {
        if (!empty($this->ydDisplays)) {
            return $this->ydDisplays;
        } else {
            $this->ydDisplays = Ad::ydDisplays();
            return $this->ydDisplays;
        }
    }
    
    public function server()
    {
        return $this->belongsTo('Server');
    }
    
    public function getPercentage()
    {
        $total = Premium::where('created_at', '>=', DB::raw('NOW() - INTERVAL 1 MONTH'))->sum('paid');
        if ($total == 0)
            $total = $this->paid;
        if ((int) $this->paid < 5)
            return 0;
        return ($this->paid / ($total + $this->paid)) * 100;
    }
    
    public function goalViews()
    {
        return round(($this->getPercentage() / 100) * $this->ydDisplays());
    }
    
    public static function estimateViews($amount)
    {
        $est       = new self();
        $est->paid = $amount;
        return $est->goalViews();
    }
    
    public function setInterval()
    {
        $this->displayInterval = round(((24 * 60) / $this->ydDisplays()) * $this->goalViews());
    }
    
    // Get a list of sponsors
    public static function getPremiums($take = 1, $rotator = false)
    {
        // If we supply a second argument, we only show servers who paid 20 or more euros
        $paidQuery = '1=1';
        if ($rotator)
            $paidQuery = 'paid >= 20';
        
        // Get the servers
        $premiums   = self::with('server', 'server.cache')->whereRaw('(' . $paidQuery . ') AND (displayInterval > 0) AND (TIMESTAMPDIFF(MINUTE, lastDisplay, UTC_TIMESTAMP()) >= displayInterval)')->where('created_at', '>=', DB::raw('NOW() - INTERVAL 1 MONTH'))->orderBy('lastDisplay', 'asc')->take($take);
        $collection = $premiums->get();
        
        // Don't use the lastDisplay if there are no servers returned
        if (empty($collection->toArray())) {
            $premiums   = Premium::with('server', 'server.cache')->whereRaw('(' . $paidQuery . ')')->where('displayInterval', '>', '0')->orderBy('lastDisplay', 'asc')->take($take);
            $collection = $premiums->get();
        }

        DB::update('UPDATE ads SET displays=displays+? ORDER BY id DESC LIMIT 1', array($take));
        $ids = array();
        
        foreach ($collection as $premium) {
            array_push($ids, $premium->id);
        }
        
        DB::table('premiums')->whereIn('id', $ids)->update( array('lastDisplay'=> DB::raw('UTC_TIMESTAMP()'), 'adDisplays' => DB::raw('adDisplays+1')) );
        // Return the collection
        return $collection->sort(function() {
			return rand(0, 2) - 1;
   		});
    }
}
