<?php
use LaravelBook\Ardent\Ardent;

class Vote extends Ardent
{
    public function server()
    {
        return $this->belongsTo('Server');
    }
}
