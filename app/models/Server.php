<?php
use Illuminate\Auth\UserInterface;
use LaravelBook\Ardent\Ardent;
use Carbon\Carbon;

class Server extends Ardent implements UserInterface
{
    public $autoPurgeRedundantAttributes = true;
    protected $guarded = array(); // Important
    public $autoHydrateEntityFromInput = true; // hydrates on new entries' validation
    public $forceEntityHydrationFromInput = true; // hydrates whenever validation is called
    public static $rules = array();
    
    public static $createRules = Array('name' => 'required|unique:servers|alpha_dash|min:4', 'password' => 'required|min:6|confirmed', 'email' => 'required|email', 'ip' => 'required', 'port' => 'required|numeric|checkStatus:ping', 'banner_file' => 'image|max:500', 'query' => 'sometimes|checkStatus:query');
    
    public static $updateRules = Array('password' => 'sometimes|min:6|confirmed', 'email' => 'required|email', 'ip' => 'required', 'port' => 'required|numeric|checkStatus:ping', 'banner_file' => 'image|max:500', 'query' => 'sometimes|checkStatus:query');
    
    public static $resetRules = Array('password' => 'required|min:6|confirmed');
    
    public function getRememberToken()
    {
        return $this->remember_token;
    }
    
    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }
    
    public function getRememberTokenName()
    {
        return 'remember_token';
    }
    
    
    /**
     * Constructor
     *
     * @return void
     */
    function __construct($attributes = array())
    {
        parent::__construct($attributes);
        
        $this->purgeFilters[] = function($key)
        {
            $purge = array(
                'keywords',
                'banner_file',
                'json'
            );
            return !in_array($key, $purge);
        };
        $this->setRules();
    }
    
    /**
     * Set validation rules
     *
     * @return void
     */
    public function setRules($update = false)
    {
        if ($update === true) {
            self::$rules = self::$updateRules;
        } elseif ($update == 'reset') {
            self::$rules = self::$resetRules;
        } elseif ($update == 'none') {
            self::$rules = array();
        } else {
            self::$rules = self::$createRules;
        }
    }
    
    /**
     * Set associations
     *
     * @return mixed
     */
    public function keywords()
    {
        return $this->belongsToMany('Keyword');
    }
    
    public function votes()
    {
        return $this->hasMany('Vote');
    }
    
    public function reviews()
    {
        return $this->hasMany('Review');
    }
    
    public function cache()
    {
        return $this->hasOne('QCache');
    }
    
    public function premium()
    {
        return $this->hasOne('Premium');
    }
    
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $primaryKey = 'id';
    protected $hidden = array('password', 'salt');
    
    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this->getKey();
    }
    
    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }
    
    /**
     * Get the e-mail address where password reminders are sent.
     *
     * @return string
     */
    public function getReminderEmail()
    {
        return $this->email;
    }
    
    /**
     * Clears the server's cache
     * 
     * @return void 
     */
    public function forget()
    {
        $name = $this->name;
        Cache::forget('cache_' . trim(strtolower($name)));
        Cache::forget('server_' . trim(strtolower($name)));
        Cache::forget('keywords_' . trim(strtolower($name)));
        Cache::forget('last_players_' . trim(strtolower($name)));
        Cache::forget('player_history_cache_' . trim(strtolower($name)));
    }
    
    /**
     * Refresh server
     * 
     * @return void
     */
    public function refresh()
    {
        $cache = QCache::firstOrNew(array('server_id' => $this->id));
        $refresh = new Refresh($this->ip, $this->port, (!empty($this->query)) ? $this->query : false, (!empty($cache->version)) ? $cache->version : false);
		$version = (!empty($cache->version)) ? $cache->version : false;
        /*
        // Get Cache
        $cache      = $this->cache();
        $cacheArray = $cache->get()->toArray();
        if (empty($cacheArray)) {
            $cache = new QCache();
            $this->cache()->save($cache);
        }
        
        // Initiate cache
        $cache = $this->cache()->first();
        */
        
        // Get data
        $data = $refresh->getData();
        
        // Initiate players
        $player = new Player;
        
        // Is it online?
        if ($data) {
            // Save players
            $player->max   = $data['maxPlayers'];
            $player->count = $data['players'];
            if (isset($data['names']))
                $player->names = json_encode($data['names']);
            
            // Load up the info
            $cache->motd    = $data['hostName'];
            $cache->version = $this->version($data['version']);
            
            // Manage version database
            $versions = json_decode(Cache::get('versions'));
            if (!is_array($versions))
                $versions = Array();
            if (!in_array($cache->version, $versions)) {
                array_push($versions, $cache->version);
                usort($versions, function($a, $b)
                {
                    return -1 * version_compare($a, $b);
                });
                Cache::forever('versions', json_encode($versions));
            }
            
            // Favicon    
            if (isset($data['favicon']))
                $cache->favicon = $data['favicon'];
            
            // Status is online
            $cache->status  = 1;
            $cache->success = (isset($cache->success)) ? $cache->success + 1 : 1;
        } else {
            // No Players cuz it's offline  
            $player->max   = 0;
            $player->count = 0;
            
            // Status is offline
            $cache->status = 0;
            $cache->fail   = (isset($cache->fail)) ? $cache->fail + 1 : 1;
        }
        
        // We need to have a database of countries
        $countries = json_decode(Cache::get('server_countries'));
        if (!is_array($countries))
            $countries = Array();
        if (!in_array(strtoupper($this->country), $countries)) {
            array_push($countries, strtoupper($this->country));
            sort($countries);
            Cache::forever('server_countries', json_encode($countries));
        }
        
        // Don't update timestamps!
        $this->timestamps = false;
        
        // We need to have a temporary value of the max players in caches
        $this->playercount = $player->count;
        $this->playermax   = $player->max;
        $this->version     = $cache->version;
        
        // Save
        $this->cache()->save($cache);
        $this->cache()->first()->players()->save($player);
        $this->uptime = $this->uptime($this->cache()->first()->fail, $this->cache()->first()->success);
        $this->score  = $this->score($this->uptime);
        $this->votes  = $this->votes()->count();
        $this->forceSave();
        $this->cache->touch();
        
        // Clear cache
        $name = $this->name;
        $this->forget();
        
        return (bool)$cache->status;
    }
    
    /**
     * Parse returned version. Used in Refresh.
     *
     * @return string
     */
    private function version($string)
    {
        $version = Array();
        preg_match_all("/((\d+)\.(\d+)(\.(\d+))*)+|\d\dw\d\d[a-z]/i", $string, $version);
        return end($version[0]);
    }
    
    /**
     * Calculate uptime percentage
     *
     * @return float
     */
    private function uptime($fail, $success)
    {
        try {
            $decval = (1 - ($fail / $success));
            if ($decval < 0)
                $decval = 0;
            return round(100 * $decval, 2);
        }
        catch (Exception $e) {
            return 0;
        }
    }
    
    /**
     * Calculate score
     *
     * @return int
     */
    private function score($uptime)
    {
        try {
            return round($uptime * ((1 / 3) * $this->created_at->diffInDays(Carbon::now())) + (3 * $this->votes()->count()));
        }
        catch (Exception $e) {
            return 0;
        }
    }
    
    /**
     * Round the score
     *
     * @return string
     */
    public function roundScore()
    {
        $score = $this->score;
        if ($score > 10000)
            $score = round($score / 1000, 1, PHP_ROUND_HALF_DOWN) . 'k';
        return $score;
    }
    
    /**
     * Determine status bar colour
     *
     * @return string
     */
    public function statusBar()
    {
        if ($this->uptime < 40) {
            return 'bad';
        } elseif ($this->uptime < 60) {
            return 'warning';
        } else {
            return 'good';
        }
    }
    
    /**
     * Check permissions
     *
     * @return bool
     */
    public function hasAuth()
    {
        return (Auth::user() && (Auth::user()->id == $this->id));
    }
    
    /**
     * Check permissions
     *
     * @return bool
     */
    public function hasPermission()
    {
        return ((Auth::user() && $this->id == Auth::user()->id) || Admin::inSession());
    }
    
    /**
     * Set keywords
     *
     * @return void
     */
    public function setKeywords($keywords)
    {
    	if (!is_array($keywords)) $keywords = Array();
    	
        $existing = $this->keywords;
        foreach ($existing as $ex) {
        	$keywordid = $ex->id;
	        if( !in_array((int)$keywordid, $keywords) ) {
		        $this->keywords()->detach($keywordid);
	        }
        }
        foreach ($keywords as $keyword) {
            if (!$this->keywords()->where('keyword_id', $keyword)->first()) {
			    $this->keywords()->attach($keyword);
            }
            
        }
    }
    
    /**
     * Get keywords
     *
     * @return array
     */
    public function getKeywords()
    {
        $k = Array();
        foreach ($this->keywords()->get() as $keyword) {
            array_push($k, $keyword->id);
        }
        return $k;
    }
    
    /**
     * Promote to Premium
     *
     * @return bool
     */
    public function addPremium($amount)
    {
        if (count($this->premium)) {
            $premium = $this->premium;
        } else {
            $premium = new Premium();
            $this->premium()->save($premium);
        }
        $premium->paid = (int) $amount;
        $premium->setInterval();
        $premium->lastDisplay = Carbon::now()->subMinutes($premium->displayInterval);
        $premium->save();
    }
    
    /**
     * Add a vote
     * 
     * @return bool
     */
    public function addVote($player = false, $ip = false)
    {
        // Create the vote
        $vote = new Vote();
        
        // Set content
        if ($player)
            $vote->player = $player;
        if (!$ip)
            $ip = Request::getClientIp();
        $vote->ip = $ip;
        
        // Save it
        if ($this->votes()->save($vote)) {
            if (!empty($this->votifier_ip) && !empty($this->votifier_port) && !empty($this->votifier_key)) {
                $votifier = new Votifier($this->votifier_key, $this->votifier_ip, $this->votifier_port, SERVERLIST_NAME);
                if (!$votifier->sendVote($player))
                    Session::flash('danger', Lang::get('server.messages.vote.votifier'));
            }
            return true;
        }
        return false;
    }
    
    /**
     * Determines if a user is eligible to vote for this server
     *
     * @return bool
     */
    public function canVote($player = false, $ip = false)
    {
        if (!$ip)
            $ip = Request::getClientIp();
        $ipvotes = $this->votes()->where('ip', '=', $ip)->orderBy('created_at', 'desc')->first();
        if ($player)
            $playervotes = $this->votes()->orderBy('created_at', 'desc')->where('player', '=', $player)->first();
        
        if (empty($ipvotes) && (!isset($playervotes) || empty($playervotes)))
            return true;
        
        if (!empty($ipvotes)) {
            $ipvotesbool = (Carbon::now()->diffInHours($ipvotes->created_at) >= 22);
        } else {
            $ipvotesbool = true;
        }
        if (isset($playervotes) && !empty($playervotes)) {
            $playervotesbool = (Carbon::now()->diffInHours($playervotes->created_at) >= 22);
        } else {
            $playervotesbool = true;
        }
        
        return ($playervotesbool && $ipvotesbool);
    }
    
    /**
     * Query for a server with the name
     *
     * @return Server
     */
    public static function name($name)
    {
        return self::where('name', $name)->first();
    }
    
    /**
     * Query for a server with the name, throw ModelNotFoundError when not found
     *
     * @return Server
     */
    public static function nameOrFail($name)
    {
        return self::where('name', $name)->firstOrFail();
    }
    
    /**
     * Query for a server with the name, eager load cache and players and cache for 5m
     *
     * @return Server
     */
    public static function withCache($name)
    {
        /* Why is it still querying? It should be cached! Looking at it later or when necessairy */
        // Should probably use JOIN here instead of Eager Loading, but since we're caching and it is
        // only for servers, that could wait a bit.
        $server = self::where('name', $name)->with(array(
            'keywords' => function($q) use ($name)
            {
                $q->remember(5, 'keywords_' . strtolower($name));
            },
            'cache' => function($q) use ($name)
            {
                $q->remember(5, 'caches_' . strtolower($name));
            },
            'cache.lastPlayers' => function($q) use ($name)
            {
                $q->remember(5, 'cache_' . strtolower($name));
            }
        ))->remember(5, 'server_' . strtolower($name))->get()->first();
        
        if (!is_null($server))
            return $server;
        App::abort(404);
    }
    
    /**
     * Generates a unique salt to be used for authentication
     *
     * @return string
     */
    private function generateSalt($length = 12, $salts = '', $secret = 'thisbesecret')
    {
        $str = '';
        $l   = 0;
        while ($l < $length) {
            $l = strlen($str);
            $str .= hash('sha512', $secret . time() . uniqid(true) . $salts);
        }
        $str = base64_encode($str);
        $str = strlen($str) > $length ? substr($str, 0, $length) : $str;
        return trim(strtr($str, '/+=', '   '));
    }
    
    /**
     * Insert a password in the database
     *
     * @return void
     */
    public function setPassword($password)
    {
        $this->salt     = $this->generateSalt('5');
        $this->password = Hash::make($password . $this->salt);
    }
    
    /**
     * Create and save unique reset string
     *
     * @return string
     */
    public function reset()
    {
        $this->reset = $this->generateSalt(32);
        $this->setRules('none');
        $this->forceSave();
        return $this->reset;
    }
    
    /**
     * Tasks executed before data is inserted into the database
     *
     * @return void
     */
    public function beforeSave()
    {
        $original = $this->getOriginal();
        if ($this->isDirty('password')) {
            if (empty($this->password)) {
                $this->password = $original['password'];
                return;
            }
            $this->setPassword($this->password);
        }
    }
    
    /**
     * Cleaning up after deletion of data
     *
     * @return void
     */
    public function afterDelete()
    {
        $this->reviews()->delete();
        $this->votes()->delete();
        $this->cache()->get()->first()->players()->delete();
        $this->cache()->delete();
        $this->keywords()->detach($this->id);
        $this->forget();
        @$this->premium()->delete();
    }
}
