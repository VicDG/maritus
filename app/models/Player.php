<?php
use LaravelBook\Ardent\Ardent;

class Player extends Ardent
{
    function cache()
    {
        return $this->belongsTo('Cache');
    }
}
