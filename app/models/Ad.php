<?php
use LaravelBook\Ardent\Ardent;
use Carbon\Carbon;

/**
This is the idea behind advertising on our serverlist software:
- A server chooses how much he wants to pay this month
Based on this amount, the total amount of paying servers and the server score, we calculate the 'display percentage'.
This is the percentage of the total amount of ad displays on the website that the server will receive during the time that it is active.
The displays that a server gets is recalculated every day based on the total amount of ad displays the day before.

A server can 'upgrade' its advertising budget. The current 'campaign' will then stop instantly and the new one will begin.
*/
class Ad extends Ardent
{
    public static function today()
    {
        return self::orderBy('id', 'desc')->take(1)->get(array('displays'))->first()->displays;
    }
    
    public static function ydDisplays()
    {
    	return self::orderBy('id', 'desc')->skip(1)->take(1)->get(array('displays'))->first()->displays;    
    }
}
