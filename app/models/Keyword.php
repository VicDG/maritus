<?php
use LaravelBook\Ardent\Ardent;

class Keyword extends Ardent
{
    public function servers()
    {
        return $this->belongsToMany('Server');
    }
}
