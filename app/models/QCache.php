<?php
use LaravelBook\Ardent\Ardent;
class QCache extends Ardent
{
    protected $table = 'caches';
    protected $guarded = array();
    public function players()
    {
        return $this->hasMany('Player', 'cache_id');
    }
    
    public function lastPlayers()
    {
        return $this->hasMany('Player', 'cache_id')->orderBy('id', 'desc')->limit(1);
    }
    
    public function server()
    {
        return $this->belongsTo('Server');
    }
}
