<?php
use LaravelBook\Ardent\Ardent;

class Review extends Ardent
{
    public function server()
    {
        return $this->belongsTo('Server');
    }
}
