<?php
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;
use LaravelBook\Ardent\Ardent;

class Admin extends Ardent
{
    
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $primaryKey = 'id';
    protected $table = 'admins';
    protected $hidden = array('password', 'salt');
    
    private function generateSalt($length = 12, $salts = '')
    {
        $str = '';
        $l   = 0;
        while ($l < $length) {
            $l = strlen($str);
            $str .= hash('sha512', 'wudduuuup' . time() . uniqid(true) . $salts);
        }
        $str = base64_encode($str);
        $str = strlen($str) > $length ? substr($str, 0, $length) : $str;
        return trim(strtr($str, '/+=', '   '));
    }
    
    public function checkLogin($password = false)
    {
        if ($password) {
            return ($this->password == hash('sha256', $password . $this->salt));
        }
        return false;
        
    }
    
    public function startSession()
    {
        Session::put('isAdmin', true);
        Session::put('adminName', $this->name);
        Session::put('adminId', $this->id);
    }
    
    public function stopSession()
    {
        Session::forget('isAdmin');
        Session::forget('adminName');
        Session::forget('adminId');
    }
    
    public static function inSession()
    {
        return (Session::get('adminId'));
    }
    
    public static function currentAdmin()
    {
        if (self::inSession()) {
            return Admin::find(Session::get('adminId'));
        }
    }
    public function beforeSave()
    {
        if ($this->isDirty('password')) {
            $this->salt     = $this->generateSalt('5');
            $this->password = hash('sha256', $this->password . $this->salt);
        }
    }
}
