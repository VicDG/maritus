<?php
use Carbon\Carbon;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
/*
use Darsain\Console\Console;
Route::get('console',  'Darsain\Console\ConsoleController@getIndex');
Route::post('console', array(
	'as' => 'console_execute',
	'uses' => 'Darsain\Console\ConsoleController@postExecute'
));
*/

// Old Minequery Redirects
Route::get('/server/vote/{name}', function($name)
{
    return Redirect::route('server.view', array(
        $name,
        'vote'
    ));
});

Route::get('/banner/{type}/{name}.png', function($type, $name)
{
    return Redirect::route('banner.big', array(
        $name
    ));
})->where(array(
    'type' => 'bg|custom|med|large|mc'
));

Route::get('/api/chart/{name}/{height}-{width}', function($name)
{
    return Redirect::route('api.chart', array(
        $name,
        200
    ));
});
Route::get('/api/sc.js', function()
{
    return Redirect::route('api.playerlist', array(
        Input::get('server')
    ));
});


// Posts
Route::post('server/create', array(
    'before' => 'currentCheck',
    'as' => 'server.create',
    'uses' => 'ServerController@create'
));

Route::post('server/update/{name}', array(
    'as' => 'server.update',
    'uses' => 'ServerController@update'
));

Route::post('server/delete/{name}', array(
    'before' => 'permission',
    'as' => 'server.remove',
    'uses' => 'ServerController@remove'
));

Route::post('reset/request', array(
    'as' => 'reset.get',
    'uses' => 'ServerController@requestReset'
));

Route::post('reset/{id}/{token}', array(
    'before' => 'resetToken',
    'as' => 'reset.set',
    'uses' => 'ServerController@setPassword'
));

Route::post('vote/create/{name}', array(
    'before' => array(
        'canVote',
        'captcha'
    ),
    'as' => 'vote.create',
    'uses' => 'VoteController@create'
));

Route::post('premium', array(
    'uses' => 'PremiumController@pay'
));

Route::post('review/{name}/post', array(
    'before' => 'captcha',
    'as' => 'review.post',
    'uses' => 'ReviewController@post'
));

Route::get('server/{name}/{tab}/{ajax}', array(
    'as' => 'server.view',
    'uses' => 'ServerController@view'
));

Route::get('api/{name}.json', array(
    'as' => 'api.playersjs',
    'uses' => 'ApiController@index'
));

Route::get('api/players/{name}.js', array(
    'as' => 'api.playersjs',
    'uses' => 'ApiController@playersjs'
));

Route::get('api/players/{name}', array(
    'as' => 'api.players',
    'uses' => 'ApiController@players'
));

Route::get('api/playerlist/{name}.js', array(
    'as' => 'api.playerlist',
    'uses' => 'ApiController@playerlist'
));

Route::get('api/badge/{name}.png', array(
    'as' => 'api.badge',
    'uses' => 'ApiController@badge'
));

Route::get('api/button/{name}.js', array(
    'as' => 'api.buttonjs',
    'uses' => 'ApiController@buttonjs'
));

Route::get('api/button/{name}', array(
    'as' => 'api.button',
    'uses' => 'ApiController@button'
));
Route::get('api/premium/{amount}', array(
    'as' => 'api.premium',
    'uses' => 'ApiController@premium'
));

Route::get('premium/estimate/{num}', function($num)
{
    return Premium::estimateViews($num);
});

Route::get('api/chart/{name}/{height}', array(
    'as' => 'api.chart',
    function($name, $height)
    {
        $server = Server::nameOrFail($name);
        return View::make('api/chart', array(
            'server' => $server,
            'height' => $height
        ));
    }
));
Route::get('/ajax', 'ListController@view');
// Group routes that need to have the premium rotator
Route::group(array(
    'before' => 'rotator'
), function()
{
    Route::get('/', 'ListController@view');
    Route::get('server/add', array(
        'before' => 'currentCheck',
        'as' => 'server.add',
        'uses' => 'ServerController@add'
    ));
    Route::get('server/edit/{name}', array(
        'before' => 'permission',
        'as' => 'server.edit',
        'uses' => 'ServerController@edit'
    ));
    Route::get('server/delete/{name}', array(
        'before' => 'permission',
        'as' => 'server.delete',
        'uses' => 'ServerController@delete'
    ));
    Route::get('server/{name}/{tab?}', array(
        'as' => 'server.view',
        'uses' => 'ServerController@view'
    ));
    Route::get('vote/{name}', array(
        'as' => 'vote.add',
        function($name)
        {
            return Redirect::route('server.view', array(
                $name,
                'vote'
            ));
        }
    ));
    Route::get('reset/request', array(
        'as' => 'reset.get',
        'uses' => 'ServerController@reset'
    ));
    Route::get('reset/{id}/{token}', array(
        'before' => 'resetToken',
        'as' => 'reset',
        'uses' => 'ServerController@choosePassword'
    ));
    
    Route::get('page/{page}', array(
        'as' => 'page',
        function($page)
        {
            return View::make('page/' . $page);
        }
    ));
    
    Route::get('premium', array(
        'as' => 'premium',
        'uses' => 'PremiumController@index'
    ));
    
    Route::get('premium/success', array(
        'as' => 'premium.success',
        'uses' => 'PremiumController@success'
    ));
    
    Route::any('login', array(
        'as' => 'login',
        function()
        {
            if (Admin::inSession()) {
                Session::flash('danger', Lang::get('server.messages.admin.warning'));
                return Redirect::intended('/');
            }
            
            $wrong = false;
            if (Request::isMethod('post')) {
                $server = Server::name(Input::get('name'));
                if ($server) {
                    if (Auth::check())
                        Auth::logout();
                    $salt = @$server->salt;
                    if (Auth::attempt(array(
                        'name' => Input::get('name'),
                        'password' => Input::get('password') . $salt
                    ), Input::get('remember'))) {
                        Auth::loginUsingId($server->id);
                        return Redirect::intended(URL::route('server.view', array(
                            Input::get('name')
                        )));
                    }
                    $wrong = true;
                }
                $wrong = true;
            }
            return View::make('login', array(
                'wrong' => $wrong
            ));
        }
    ));
    
    Route::any('admin/login', array(
        'as' => 'admin.login',
        function()
        {
            if (Admin::inSession()) {
                return Redirect::intended('/');
            }
            
            $wrong = false;
            if (Request::isMethod('post')) {
                $admin = @Admin::where('name', '=', Input::get('name'))->first();
                if ($admin && Input::get('name')) {
                    if ($admin->checkLogin(Input::get('password'))) {
                        if (Auth::check())
                            Auth::logout();
                        $admin->startSession();
                        Session::flash('success', Lang::get('server.messages.admin.success'));
                        return Redirect::intended('/');
                    }
                }
                $wrong = true;
            }
            return View::make('admin/login', array(
                'wrong' => $wrong
            ));
        }
    ));
    
    Route::any('admin/premium/{name}', array(
        'before' => 'isAdmin',
        'as' => 'admin.premium',
        function($name)
        {
            $server  = Server::nameOrFail($name);
            $premium = $server->premium;
            if (!count($premium))
                $premium = false;
            
            if (Request::isMethod('post')) {
                if (Input::get('paid') >= 1) {
                    $server->addPremium(Input::get('paid'));
                } else {
                    if ($premium) {
                        $server->premium()->delete();
                    }
                }
                return Redirect::route('server.view', array(
                    $name
                ));
            }
            
            return View::make('admin/premium', array(
                'premium' => $premium
            ));
        }
    ));
    
    Route::get('avatar/{player}/{size?}', array(
        'as' => 'avatar',
        function($player, $size = 32)
        {
            include app_path() . '/lib/Minepic.php';
            $avatar = new Minepic;
            
            $response = Response::make($avatar->avatar($player, $size));
            $response->header('Content-Type', 'image/png');
            $response->header('Cache-Control', 'max-age=300');
            $response->header('Expires', gmdate('D, d M Y H:i:s \G\M\T', time() + 300));
            return $response;
        }
    ));
});

Route::any('logout', array(
    'as' => 'logout',
    function()
    {
        if (Auth::check()) {
            Auth::logout();
        }
        return Redirect::intended('/');
    }
));

Route::any('admin/logout', array(
    'as' => 'admin.logout',
    function()
    {
        if (Admin::inSession()) {
            $admin = Admin::find(Session::get('adminId'));
            $admin->stopSession();
            Session::flash('success', Lang::get('server.messages.admin.logout.success'));
        }
        return Redirect::intended('/');
    }
));
Route::get('favicon/{name}.png', array(
    'as' => 'favicon',
    function($name)
    {
        $server   = Server::nameOrFail($name);
        if(file_exists(public_path() . '/' . 'favicon_cache' . '/' . $server->id . '.png')) {
	        $path = public_path()  . '/' . 'favicon_cache' . '/' . $server->id . '.png';
        }
        else {
	        $path = public_path()  . '/' . 'favicon_cache' . '/' . 'default' . '.png';
        }
        $response = Response::make(file_get_contents($path, 200));
        $response->header('Content-Type', 'image/png');
        $response->header('Cache-Control', 'max-age=300');
        $response->header('Expires', gmdate('D, d M Y H:i:s \G\M\T', time() + 300));
        return $response;
    }
));    
Route::get('banner/{name}.png', array(
    'as' => 'banner.small',
    function($name)
    {
        $server   = Server::nameOrFail($name);
        $response = Response::make(file_get_contents(Banner::location($server, 'small', public_path())), 200);
        $response->header('Content-Type', 'image/png');
        $response->header('Cache-Control', 'max-age=300');
        $response->header('Expires', gmdate('D, d M Y H:i:s \G\M\T', time() + 300));
        return $response;
    }
));
Route::get('banner/big/{name}.png', array(
    'as' => 'banner.big',
    function($name)
    {
        $server   = Server::nameOrFail($name);
        $response = Response::make(file_get_contents(Banner::location($server, 'big', public_path())), 200);
        $response->header('Content-Type', 'image/png');
        $response->header('Cache-Control', 'max-age=300');
        $response->header('Expires', gmdate('D, d M Y H:i:s \G\M\T', time() + 300));
        return $response;
    }
));
    
Route::post('captcha/handshake', function()
{
    if (Input::has('action') && Input::has('captcha_key')) {
        if (htmlentities(Input::get('action'), ENT_QUOTES, 'UTF-8') == 'captcha') {
            Session::put('captcha_key', Input::get('captcha_key'));
            return Response::json(Array(
                'error' => false
            ));
        }
    }
    return Response::json(Array(
        'error' => true
    ));
});

Route::post('editor/upload', function()
{
    $tmpname = $_FILES["img"]['tmp_name'];
    
    // It needs to be an image
    $allowedExts = array("gif", "jpeg", "jpg", "png");
    $extension = end($allowedExts);
    if(($_FILES["img"]["type"] == "image/gif" || $_FILES["img"]["type"] == "image/jpeg" || $_FILES["img"]["type"] == "image/jpg" || $_FILES["img"]["type"] == "image/pjpeg" || $_FILES["img"]["type"] == "image/x-png" || $_FILES["img"]["type"] == "image/png") && in_array($extension, $allowedExts) ) {
        if ($_FILES["img"]["error"] > 0) {
            echo '{"status":0,"msg":"'. $_FILES["img"]["error"] .'"}';
        }
        elseif ( ($_FILES["img"]["size"] / 1024) > 500 ) {
            echo '{"status":0,"msg":"The image cannot be larger than 500KB."}';
        }
        else {
            $path = '/' . 'uploads' . '/' . 'editor' . '/' . str_random(12) . '.' . $extension;
            move_uploaded_file($tmpname, public_path() . $path);
            echo '{"status":1,"msg":"OK","image_link":"'.$path.'","thumb_link":"'.$path.'"}';
        }
    }
    else {
        echo '{"status":0,"msg":"The file you supplied is not an image."}';
    }
    
});

Route::get('sitemap', function() 
{
    $sitemap = App::make('sitemap');
    if (!Cache::has('sitemap')) {
        $sitemap->add(URL::to('/'), Carbon::now(), 1.0, 'always');
        $sitemap->add(URL::to('/add'), Carbon::now(), 0.8, 'monthly');
        $sitemap->add(URL::to('/login'), Carbon::now(), 0.7, 'monthly');
        $sitemap->add(URL::to('/premium'), Carbon::now(), 0.8, 'monthly');
        $sitemap->add(URL::to('/page/help'), Carbon::now(), 0.3, 'weekly');
        $sitemap->add(URL::to('/page/tos'), Carbon::now(), 0.2, 'yearly');
        
        foreach (Server::get(array('name')) as $server) {
          $sitemap->add(URL::route('server.view', array($server->name)), Carbon::now(), null, 'weekly');
        }
        
        $xml = $sitemap->generate();
        Cache::put('sitemap', json_encode($xml), 10080);
        return Response::make($xml['content'], 200, $xml['headers']);
    }
    else {
        $xml = json_decode(Cache::get('sitemap'), true);
        return Response::make($xml['content'], 200, $xml['headers']);
    }
    
});