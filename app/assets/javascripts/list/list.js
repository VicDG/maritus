/*
  Because of the jQuery plugins used I had to write spaghetti code, but at least it works (and is documented enough).
*/

// We need this shit because Javascript sucks (not really)
Array.prototype.removeItem = function (item) {
  var deleted;
  var index = 0;
  
  while ((index = this.indexOf(item, index)) != -1) {
    deleted = this.splice(index, 1);
  }

  if (deleted) {
    return deleted[0];
  }
};

// Clicking on the tablerow should redirect to the server view
function divLink() {
  $('tr[href]').click(function() {
    window.open($(this).attr("href"));
  });
  
  // Safari opens the page two times if you click the link
  // Still, keep the link for non js fallback, just disable it if js is enabled
  
  $('.selector-ip').tooltip({ placement: 'bottom' });
}

// Define our global variables
var currentPage;
var lastPage = false;
var change = false;
var busy = false;
// Relisting function, used to rebuild the list after filtering
function reList(bypass) {
  // There need to be changes
  if(change == false && bypass !== true) return false;
  
  // Preload
  $('#h-preload').show();
    
  // Reset page for infinite scroll
  currentPage = 0;
  
  // Get it
  $.ajax({
    type: "GET",
    cache: false,
    contentType: false,
    processData: false,
    url: '/ajax?page=' + (parseInt(currentPage) + parseInt(1)) + '&ajax',
    beforeSend: function() {
      // Set variable to avoid double requests
      busy = true;
    },
    success: function(data) {
      if($.trim(data).length === 0) {
        // There's no data
        lastPage = true;
        $('#list').html(''); 
      }
      else {
        lastPage = false;
        // There os data
        $('#list').html(data);
        divLink();
        
        // Normal page shit
        currentPage = currentPage + 1;
      }
      
      // Done loading
      $('#h-preload').hide();
  
      // Idle
      busy = false;
      change = false;
      
      // Footer
      resizeFooter();
      
      // Save current list
      /*
      lscache.set('table-cache', { currentPage: currentPage, html: $('#list').html() }, 2);
      */
      
      // Refloat the boat
      $(document.body).trigger("sticky_kit:recalc");
    }
  });
}

// Horrible solution for empty space on small table
function resizeFooter() {
  var space = window.innerHeight - $('#end2').offset()['top'];
  if(space > 104) {
    $('#end2').css({ height: space }); 
  }
  else {
    $('#end2').css({ height: 'auto' }); 
  }
   
}
jQuery(window).on('resize', function() {
  resizeFooter();  
});

// Function for resetting
function resetFilters() {
  // Remove selections
  $('select.multiselect').each(function() {
    var select = $(this);
    select.each(function() {
      if($(this).val() !== null) {
        select.multiselect('deselect', $(this).val());
      }
    });
  });
  
  // Delete cookies
  $.removeCookie('sort_direction', { path: '/' });
  $.removeCookie('sort', { path: '/' });
  $.removeCookie('filter_countries', { path: '/' });
  $.removeCookie('filter_population', { path: '/' });
  $.removeCookie('filter_version', { path: '/' });
  $.removeCookie('filter_keywords', { path: '/' });
  
  // These are the default selections, also used in the select defenitions
  $('#direction').multiselect('select', 'desc');
  $('#sort').multiselect('select', 'votes');
        
  // Reload the table, bypass changecheck
  reList(true);
  
  // Footer space
  resizeFooter();
  
  // Refloat the boat
  $(document.body).trigger("sticky_kit:recalc");
}

// Set our shit up
jQuery(document).ready(function($) {
  /*
    Restore last page
  */
  /*
    Before I can implement this localstorage caching system I first need to:
    - set a cookie that tells PHP to not get any data (NEEDS TO HAVE SAME EXP DATE)
    - make a system that saves to where you scrolled last time, and a system to restore that WITHOUT LAG
    THIS IS NOT URGENT BUT WOULD BE HELLA COOL
  */
  /*
  var tableCache = lscache.get('table-cache');
  if(tableCache) {
    currentPage = tableCache.currentPage;
    $('#list').html(tableCache.html);
  }
  */
  /* ./ Restore Last Page */
  
  /*
    Misc
  */
  $('.selector').tooltip({ placement: 'bottom' });
  $('.reset-tooltip').tooltip({ placement: 'left' });
  $('#votetop').stick_in_parent();
  divLink();
  resizeFooter();
  /* ./ Misc */

  /*
    Infinite Scrolling
  */
  // We want to be able to just add to an existing page (I guess?)
  var currentURL = document.URL;
  var URLData = currentURL.match(/\?page=(\d*)/);
  if(URLData == null) {
    currentPage = 1;
  }
  else {
    currentPage = URLData[1];
  }
  /* ./ Infinite Scrolling */
              
  /* 
    Sorting shit
  */
  $('select#direction.multiselect').multiselect({
    buttonText: function(options, select) {
      return  '<i class="glyphicon glyphicon-sort"></i> <b class="caret"></b>';
    },
    selectedClass: null,
    checkboxName: 'direction',
    templates: {
      button: '<button id="direction_button" type="button" class="multiselect dropdown-toggle" data-toggle="dropdown"></button>'
    },
    onChange: function(element, selected) {
      $.cookie('sort_direction', element.attr('value'), { expires: 10000, path: '/' }); 
      reList(true);
    }
  });
  if (typeof $.cookie('sort_direction') != 'undefined') {
    var direction = $.cookie('sort_direction');
    $('#direction').multiselect('select', direction);
  }
  else {
    $('#direction').multiselect('select', 'desc');
  }

  $('select#sort.multiselect').multiselect({
    selectedClass: null,
    checkboxName: 'sort',
    onChange: function(element, selected) {
      $.cookie('sort', element.attr('value'), { expires: 10000, path: '/' });
      reList(true); 
    }
  });
  if (typeof $.cookie('sort') != 'undefined') {
    var sort = $.cookie('sort');
    $('#sort').multiselect('select', sort);
  }
  else {
    $('#sort').multiselect('select', 'votes');
  }
  
  $('select#countries.multiselect').multiselect({
    enableFiltering: true,
    enableCaseInsensitiveFiltering: true,
    maxHeight: 400,
    templates: {
      li: '<li><a href="javascript:void(0);"><label class="country-check"></label></a></li>'
    },
    onDropdownHide: function(event) {
      reList();
    },
    selectedClass: null,
    buttonText: function(options, select) {
      return 'Country' + ' <b class="caret"></b>';
    },
    onChange: function(element, selected) {
      change = true;
      $.removeCookie('filter_countries', { path: '/' });
      if(selected === true && $.inArray(element.attr('value'), filter_countries) == -1) {
        filter_countries.push(element.attr('value'));
      }
      else if(selected === false) {
        filter_countries.removeItem(element.attr('value'));
      }
      
      $.cookie('filter_countries', JSON.stringify(filter_countries), { expires: 10000, path: '/' });
    }
  });

  if (typeof $.cookie('filter_countries') != 'undefined') {
    var filter_countries = JSON.parse($.cookie('filter_countries'));
    $.each(filter_countries, function(index, value) {
      $('#countries').multiselect('select', value);
    });
  }
  else {
    var filter_countries = [];
  }
  
  $('select#population.multiselect').multiselect({
    onDropdownHide: function(event) {
      reList();
    },
    selectedClass: null,
    buttonText: function(options, select) {
      return 'Population' + ' <b class="caret"></b>';
    },
    onChange: function(element, selected) {
      change = true;
      $.removeCookie('filter_population', { path: '/' });
      if(selected === true && $.inArray(element.attr('value'), filter_population) == -1) {
        filter_population.push(element.attr('value'));
      }
      else if(selected === false) {
        filter_population.removeItem(element.attr('value'));
      }
      
      $.cookie('filter_population', JSON.stringify(filter_population), { expires: 10000, path: '/' });
    }
  });

  if (typeof $.cookie('filter_population') != 'undefined') {
    var filter_population = JSON.parse($.cookie('filter_population'));
    $.each(filter_population, function(index, value) {
      $('#population').multiselect('select', value);
    });
  }
  else {
    var filter_population = [];
  }

  $('select#version.multiselect').multiselect({
    enableFiltering: true,
    maxHeight: 400,
    onDropdownHide: function(event) {
      reList();
    },
    selectedClass: null,
    buttonText: function(options, select) {
      return 'Version' + ' <b class="caret"></b>';
    },
    onChange: function(element, selected) {
      change = true;
      $.removeCookie('filter_version', { path: '/' });
      if(selected === true && $.inArray(element.attr('value'), filter_version) == -1) {
        filter_version.push(element.attr('value'));
      }
      else if(selected === false) {
        filter_version.removeItem(element.attr('value'));
      }
      
      $.cookie('filter_version', JSON.stringify(filter_version), { expires: 10000, path: '/' });
    }
  });
  if (typeof $.cookie('filter_version') != 'undefined') {
    var filter_version = JSON.parse($.cookie('filter_version'));
    $.each(filter_version, function(index, value) {
      $('#version').multiselect('select', value);
    });
  }
  else {
    var filter_version = [];
  }

  $('select#keywords.multiselect').multiselect({
    enableFiltering: true,
    enableCaseInsensitiveFiltering: true,
    onDropdownHide: function(event) {
      reList();
    },
    maxHeight: 400,
    selectedClass: null,
    buttonText: function(options, select) {
      return 'Keywords' + ' <b class="caret"></b>';
    },
    onChange: function(element, selected) {
      change = true;
      $.removeCookie('filter_keywords', { path: '/' });
      if(selected === true && $.inArray(element.attr('value'), filter_keywords) == -1) {
        if(element.attr('value') != 'any' && $.inArray('any', filter_keywords)!==-1) {
          filter_keywords.removeItem('any');
          $('select#keywords.multiselect').multiselect('deselect', 'any');
        }
        filter_keywords.push(element.val());
      }
      else if(selected === false) {
        filter_keywords.removeItem(element.attr('value'));
      }
      
      if($.inArray('any', filter_keywords)!==-1) {
        filter_keywords = ['any'];
        $('select#keywords.multiselect').multiselect('clearSelection');
        $('select#keywords.multiselect').multiselect('select', 'any');
      }
      $.cookie('filter_keywords', JSON.stringify(filter_keywords), { expires: 10000, path: '/' });
    }
  });
  if (typeof $.cookie('filter_keywords') != 'undefined') {
    var filter_keywords = JSON.parse($.cookie('filter_keywords'));
    $.each(filter_keywords, function(index, value) {
      $('#keywords').multiselect('select', value);
    });
  }
  else {
    var filter_keywords = [];
  }
  /* . / Sorting */
  
  // Add flags to country dropdown
  $('label.country-check').each(function() {
    $(this).prepend('<img src="" style="margin-top: -3px;" class="flag flag-' + $.trim($(this).text().toLowerCase()) + '" /> ');
  });
  
  $('#filterbar').show();
});

// Infinite scroll event
jQuery(window).scroll(function() { 
  if (($(window).scrollTop() >= $(document).height() - $(window).height() - 20) && lastPage === false && busy === false) {
    // Preload
    $('#h-preload').show();
    
    $.ajax({
      type: "GET",
      cache: false,
      contentType: false,
      processData: false,
      url: '/ajax?page=' + (parseInt(currentPage) + parseInt(1)) + '&ajax',
      beforeSend: function() {
        busy = true;
      },
      success: function(data) {
        if($.trim(data).length === 0) {
          lastPage = true;
        }
        else {
          $('#list').append(data);
          divLink();
          currentPage = currentPage + 1;
          
        }
        busy = false;
        
        // Preload
        $('#h-preload').hide();
        
        // Save current list
        /*
        lscache.set('table-cache', { currentPage: currentPage, html: $('#list').html() }, 2);
        */
        
        // Refloat the boat
        $(document.body).trigger("sticky_kit:recalc");
      }
    });
  }
});

// Reset them filters
jQuery('#reset-filters').click(function() {
  lastPage = false;
  resetFilters();
});