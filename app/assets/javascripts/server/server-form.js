jQuery(document).ready(function($) {
    $('form#server').on('submit', function(event) {
        event.preventDefault();

        // The description textarea requires syncing which was originally done onSubmit, but we prevented that
        $('#description').sync();

        var parent = $(this).parent();

        $('.validation-error').hide();
        $('.has-error').removeClass('has-error');
        $('#errors_container').addClass('hidden');
        $('#errors').html('');

        var formdata = new FormData($(this)[0]);

        $(this).hide(800);
        $('#preload').css({
            'height': $(window).height() / 2
        });
        $('#preload').hide();
        $('#preload').removeClass('hidden');
        $('#preload').fadeIn(100, function() {
            $.ajax({
                type: "POST",
                cache: false,
                contentType: false,
                processData: false,
                url: $('form#server').prop('action') + "?json=1",
                data: formdata,
                success: function(data) {
                    if (data.status == 'success') {
                        window.location = data.redirect;
                    } else {
                        var errors = data.errors;
                        $('form#server').show(800);
                        $('#preload').hide();
                        $('#preload').addClass('hidden');
                        $('#preload').fadeOut(function() {

                        }, 100);
                        var captcha = $('.captcha').first();
                        captcha.find('div').remove();
                        captcha.clone().insertAfter(captcha);
                        captcha.remove();
                        $('.captcha').captcha({
                            autoSubmit: true,
                            autoRevert: true,
                            txtLock: '',
                            txtUnlock: ''
                        });
                        $.each(errors, function(field, error) {
                            $('#' + field).parent().parent().addClass('has-error');
                            $('#errors').append('<li>' + error + '</li>');
                            $('#errors_container').removeClass('hidden');
                            if (field == 'password') {
                                field = 'password_confirmation';
                                $('#' + field).parent().parent().addClass('has-error');
                            }
                            $('#' + field).after('<br/><div class="validation-error alert alert-danger">' + error + '</div>');
                        })
                    }
                }
            });
        });


        // Prevent hard submit
        return false;
    });
});