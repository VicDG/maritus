// Listen for tab click
$(document).ready(function() {
    function init() {

        $('.captcha').captcha({
            autoSubmit: true,
            autoRevert: true,
            txtLock: '',
            txtUnlock: ''
        });
        $.ajax({ url: 'https://platform.twitter.com/widgets.js', dataType: 'script', cache:true});
    }

    var lock = false;
    init();

    function goTab(tab, elem, back) {
        $.ajax({
            url: ajaxbase + '/' + tab + '/' + 'ajax',
            success: function(data) {
                $('#view-content').html(data);
                $('#preload').addClass('hidden');
                $('#view-content').removeClass('hidden');
                elem.tab('show');
                init();
                $('.avatar').tooltip();

                if (back != true) {
                    lock = true;
                    History.pushState({
                        tab: elem.attr('class')
                    }, title + ' - ' + elem.text(), ajaxbase + '/' + tab); // logs {state:1}, "State 1", "?state=1"
                    lock = false;
                }
            }
        })
    }

    $('#view-nav a[rel]').click(function(e) {
        e.preventDefault();

        $('#view-content').addClass('hidden');
        $('#preload').css({
            'height': $(window).height() / 2
        });
        $('#preload').removeClass('hidden');
        var tab = $(this).attr('class');

        // Ajax That shit!
        goTab(tab, $(this));
    });

    $('#vote-button').click(function(e) {
        e.preventDefault();
        $('#vote').click();
    });

    History.Adapter.bind(window, 'statechange', function() { // Note: We are using statechange instead of popstate
        if (lock == false) {
            var state = History.getState(); // Note: We are using History.getState() instead of event.state
            console.log(state);
            tab = state.data.tab;
            goTab(tab, $('a[rel]]#' + tab), true);
        }
    });
});
