if ($(window).width() >= 1280) {
    $(document).scroll(function() {
        if ($(document).scrollTop() >= 152) {
            $('body').css('background-attachment', 'fixed');
            $('body').css('background-position', 'center center');
        } else {
            $('body').css('background-attachment', 'scroll');
            $('body').css('background-position', 'center top');
        }
    });
}