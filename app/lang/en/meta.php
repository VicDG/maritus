<?php
return array(
  'description' => array(
    'home' => SERVERLIST_NAME . ' is a ' . SERVERLIST_GAME . ' serverlist. Find the best ' . SERVERLIST_GAME . ' server using our advanced filtering system or add your own to get more players. Server owners can use our API and display queried information on their own website.',
    'login' => 'Log into your ' . SERVERLIST_GAME . ' server to edit its attributes.',
    'add' => 'Add your ' . SERVERLIST_GAME . ' server to ' . SERVERLIST_NAME . ' and expose your server to millions of players. More ' . SERVERLIST_GAME . ' players guaranteed!',
    'reset' => 'Forgot your ' . SERVERLIST_GAME . ' server password? No problem! You can reset your password and you\'ll be logged in again in not time.',
    'default' => '',
    'reviews' => 'See what people are saying about the :name ' . SERVERLIST_GAME . ' server. These ' . SERVERLIST_GAME . ' server reviews can be used to decide whether or not you want to play on it.',
    'vote' => 'Vote for the :server ' . SERVERLIST_GAME . ' server on the ' . SERVERLIST_NAME .  '' . SERVERLIST_GAME . ' serverlist to get them in the spotlight and help them get more ' . SERVERLIST_GAME . ' players!',
    'players' => 'Recent players on the :name ' . SERVERLIST_GAME . '. See who is and who was online using our playerlist and ' . SERVERLIST_GAME . ' player graph which server owners can embed on their own website.',
    'banners' => 'Use these unique dynamicly generated ' . SERVERLIST_GAME . ' server banners to advertise your server on forums, or even use it as a status indicator.',
    'premium' => 'Get Premium and put your ' . SERVERLIST_GAME . ' server in the spotlight. Our unique Premium system allows server owners like you to increase their server\'s popularity. You decide how much you pay, and we expose your server accordingly. More ' . SERVERLIST_GAME . ' players guaranteed!'
  )
);