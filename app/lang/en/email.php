<?php
$content = Array();
$content['reset'] = <<<EOT
<p>Someone requested a password reset for your server, <b>:name</b>, on :serverlist_name.</p>
<p>If this was by accident or you didn't ask for a reset, you can just ignore this email.</p>
<p>The link below will redirect you to a page on which you can choose your new password.</p>
<a href=":link">:link</a>
EOT;
$content['premium']['expired'] = <<<EOT
<p>Hi,</p>
<p>Your server's (:name) Premium rank on :serverlist_name just expired.</p>
<p>If you want to, you can repurchase this rank at :link</p>
EOT;
$content['premium']['new'] = <<<EOT
<p>Hi,</p>
<p>Your server, :name, has just received Premium status for one month on :serverlist_name.</p>
<p>You will receive an email when this rank expires.</p>
<p>Thanks to your donation of €:amount, we can keep providing this service.</p>
EOT;
return $content;
