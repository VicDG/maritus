<?php
return array(
  'list' => array(
    'intro' => Array(
      'title' => 'Welcome to ' . SERVERLIST_NAME,
      'text' => SERVERLIST_NAME . ' is a ' . SERVERLIST_GAME . ' serverlist with a heart for both players and owners of ' . SERVERLIST_GAME . ' servers. We help players find - and owners promote - their perfect server. You will not only find the top ' . SERVERLIST_GAME . ' servers that you might already know, but also a myriad of smaller (size doesn\'t matter!) servers from all over the world. We know that everyone likes to play a little differently, so we built an easy-to-use server filtering system so that you can find the best ' . SERVERLIST_GAME . ' server fitting your needs.'
    ),
    'premium' => Array(
      'title' => 'Expose Your Server, Pay What You Want',
      'text' => 'Our unique Premium system allows ' . SERVERLIST_GAME . ' server owners like you to increase their server\'s popularity. You decide how much you pay, and we expose your ' . SERVERLIST_GAME . ' server accordingly.'
    ),
    'slogan' => Array(
      'premiumlove' => 'These ' . SERVERLIST_GAME . ' servers help us providing this service. (We <i class="glyphicon glyphicon-heart"></i> You)'
    ),
    'titles' => Array(
      'premium' => 'Premium ' . SERVERLIST_GAME . ' Servers',
      'votes' => 'Most Voted ' . SERVERLIST_GAME . ' Servers',
      'players' => 'Most Played ' . SERVERLIST_GAME . ' Servers',
      'score' => 'Highest Scoring ' . SERVERLIST_GAME . ' Servers',
      'uptime' => 'Highest Uptime ' . SERVERLIST_GAME . ' Servers',
      'created' => 'Last Added ' . SERVERLIST_GAME . ' Servers',
      'updated' => 'Last Updated ' . SERVERLIST_GAME . ' Servers'
    ),
    'sort' => Array(
      'tooltip' => array(
        'filter' => 'Use these dropdowns to filter the servers. If nothing is selected, all data is shown and nothing gets filtered out.',
        'reset' => 'Resets the filters to default.'
      ),
      'ascending' => 'Ascending',
      'descending' => 'Descending',
      'cozy' => 'Cozy Server (between 0 and 50 players)',
      'big' => 'Big Server (between 50 and 100 players)',
      'massive' => 'Massive Server (more than 100 players)'
    )
  ),
  'premium' => array(
    'titles' => array(
      'wantpay' => 'How much do you want to pay?'
    ),
    'strings' => array(
      'views' => 'views',
      'buy' => 'Buy'
    ),
    'text' => array(
      'approximation' => 'Enter the amount below, and you will be presented with an approximation of the amount of times per day that your server will be displayed in the premium section, and banner rotation.',
      'length' => 'After we receive your payment, your server will be premium for one month.',
      'thanks' => 'We\'d like to thank you in advance for supporting our serverlist. Thanks to you we can keep providing this service.',
    )
  ),
  'review' => array(
    'slide' => 'Slide to Review'
  ),
  'text' => array(
    'wrong' => 'Something went wrong, please try again.',
    'submit' => 'Slide to Submit'
  ),
  'reset' => array(
    'both' => 'Fill in both fields!'
  )
);