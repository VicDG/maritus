<?php 

return array(
  'messages' => array(
    'register' => array(
      'success' => 'You are now registered! Please log in',
      'error' => '<b>Oops! There were some problems processing your registration</b><br/>',
      'help' => array(
        'name' => 'This will be used for logging into the serverlist. You can use alphanumeric characters, dashes and underscores.',
        'ip' => 'This will be displayed as your server IP',
        'port' => 'If you\'re using an SRV record or you don\'t have to fill in a port whilst connecting to your server, use 25565',
        'query' => 'If you have set up the Minecraft Query protocol, you can enter the port here'
      ),
      'tos_warn' => 'By submitting this page, you agree to our Terms of Service'
    ),
    'edit' => array(
      'success' => 'Your server was edited!',
      'error' => '<b>Oops! There were some problems processing your update</b><br/>'
    ),
    'delete' => array(
      'success' => 'Your server was deleted',
      'confirm' => 'Please enter your password to confirm your deletion request',
      'incorrect' => 'Your password was incorrect'
    ),
    'login' => array(
      'current' => 'Logged in as :name',
      'incorrect' => 'Either your password or your username was incorrect',
      'create' => 'Don\'t have an account? :link',
      'already' => 'Log out from your current server first in order to add another one'
    ),
    'logout' => array(
    ),
    'review' => array(
      'missing' => 'You need to enter a username and a comment',
      'success' => 'Your review has been posted.'
    ),
    'vote' => array(
      'already' => 'You can\'t vote for this server, because you already did in the last 24 hours',
      'votifier' => 'Failed to notify Votifier. The public key is probably incorrect',
      'premium' => 'You need to have a premium Minecraft account to vote',
      'success' => 'You have successfully voted for :server',
      'fail' => 'Something went wrong whilst voting, try again',
      'slide' => 'Slide to Vote'
    ),
    'admin' => array(
      'warning' => 'You shouldn\'t log in as a server whilst being logged in as an admin.',
      'success' => 'Welcome back, dear admin!',
      'logout' => array(
        'success' => 'Successfully logged out! Bye admin!'
      )
    ),
    'reset' => array(
      'incorrect' => 'The server that you\'re trying to reset does not exist, or the email address is not associated with that server.',
      'sent' => 'We sent you an email containing further instructions on how to reset your password.',
      'success' => 'Password changed. You can now login using your new account details.',
      'malformed' => 'Heh, it seems like the reset parameters are malformed or incorrect, please try re-requesting a password reset.',
      'forgot_password' => 'Forgot password? :link'
    ),
    'premium' => array(
      'logged' => 'You need to be logged in to purchase premium for your server.',
      'invalidamount' => 'Invalid amount. Must be less than or equal to 50 euros, and more than or equal to 1 euro. Only natural numbers are allowed.',
      'success' => 'Your server was successfully promoted to Premium!'
    )
  ),
  
  'strings' => array(
    'statistics' => 'Statistics',
    'vote' => 'Vote', // Infinitive
    'players' => 'Players',
    'version' => 'Version',
    'since' => 'Since',
    'score' => 'Score',
    'uptime' => 'Uptime',
    'last_poll' => 'Last Poll',
    'votes' => array(
      'votes' => 'Votes', // Noun, plural
      'recent' => 'Recent Votes',
      'none' => 'This server does not have any votes yet'
    ),
    'votifier' => array(
      'uses' => 'Votifier supported'
    ), 
    'name' => 'Name',
    'keywords' => 'Keywords',
    'website' => 'Website',
    'manage' => 'Manage',
    'edit' => 'Edit',
    'email' => 'Email',
    'register' => 'Register',
    'premium' => 'Premium',
    'delete' => 'Delete',
    'description' => 'Description',
    'banners' => 'Banners',
    'reviews' => 'Reviews',
    'login' => 'Log In',
    'logout' => 'Log Out',
    'banner' => 'Banner',
    'generated_banners' => 'Generated Banners',
    'information' => ':serverlist_game Server Information',
    'remember_me' => 'Remember Me',
    'password' => 'Password',
    'retype_password' => 'Retype Password',
    'server_name' => 'Server Name',
    'embed' => array(
      'badge' => 'You can use this code to display a status badge on your website',
      'playerlist' => 'You can use this code to display your player history on your own website',
      'vote' => 'You can use this code to display a vote button on your website'
    ),
    'players_tab' => array(
      'offline' => 'This server is offline',
      'noquery' => '<p>This server did not enter a query port, so we cannot get player data.</p><hr/><p>If you are the owner of this server, please go to the server settings and add your query port. You can find instruction on how to set up this query port on :link.</p>',
      'none' => 'There are no online players',
      'noaccess' => 'Couldn\'t get players or no access to data'
    ),
    'player_history' => 'Player History',
    'submit' => 'Submit',
    'minecraft_username' => 'Minecraft Username',
    'review_author' => 'Posted by :player on :date',
    'add_server' => 'Add Server',
    'your_server' => 'Your Server',
    'register_titles' => array(
      'email' => 'We need your email to inform you about what\'s happening to your server',
      'password' => 'Choose a password to use for managing this server',
      'information' => 'We\'re gonna need some more information on this server',
      'connect' => 'How will we connect to your server?',
      'votifier' => 'Votifier details (completely optional)',
      'keywords' => 'Help people find your server by supplying keywords',
      'all' => 'That\'s all we need to know'
    ),
    'upload_banner' => 'Upload Banner',
    'server_country' => 'Server Country',
    'query_port' => 'Query Port',
    'port' => 'Port',
    'tos' => 'Terms Of Service',
    'toggle_navigation' => 'Toggle Navigation',
    'password_reset' => 'Password Reset',
    'reset_password' => 'Reset Password',
    'reset' => 'Reset',
    'view' => 'View', /* In context of 'View this server' (in a dropdown) */ 
    'help' => 'Help'
  )

);