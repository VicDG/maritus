<?php
$content = Array();
$content['tos'] = <<<EOT
  <p>By using :serverlist_name, you agree to our Terms of Service.</p>
  <hr/>
  <p>These are the Terms of Service of :serverlist_name.</p>
  <hr/>
  <h3>Servers on :serverlist_name</h3>
  <p>:serverlist_name is composed of user submitted :serverlist_game Servers.</p>
  <hr/>
  <ol>
    <li>
      <h4>Terms of Service</h4>
      <p>We reserve the right to remove a server at any time.</p>
    </li>
    <li>
      <h4>Adding Servers</h4>
      <p>When adding your :serverlist_game server, you agree that you are the owner of the server you are adding, and that you will add your server only once.</p>
    </li>
    <li>
      <h4>Periodic Server Check</h4>
      <p>After adding your server a connection check will be performed every 5 minutes to determine your server's status and retrieve the list of online players, the amount of online players, the motd, the favicon and the server version.</p>
    </li>
    <li>
      <h4>Server Deletion</h4>
      <p>You can delete your :serverlist_game server at any time by clicking 'Delete' in the manage menu in the upper right corner of your server page.</p>
    </li>
    <li>
      <h4>Banners</h4>
      <p>:serverlist_name allows server owners to upload their server banners. Do not use copyrighted images that you do not own or are unauthorized to use. Banners with constant flashing and alternating colors, pornography, offensive language, or gory content are not allowed.</p>
    </li>
    <li>
      <h4>MOTD</h4>
      <p>Your server's message of the day will be displayed on certain pages. You are not allowed to place advertisements or offensive language in your motd.</p>
    </li>
    <li>
      <h4>Voting for Servers</h4>
      <p>:serverlist_name makes use of a voting system to boost the score of a server which contributes to the ranking of the server on :serverlist_name. You agree that you will not participate in, promote, encourage in any way, or allow, the act of artificially adding votes to any server. Each visitor who visits :serverlist_name may vote once per 24 hours for a particular server.</p>
    </li>
    <li>
      <h4>Changes to the Terms of Service</h4>
      <p>We reserve the right to change the Terms of Service at anytime with or without notice. </p>
    </li>
  </ol>
  <hr/>
  <p>Last change: 29-06-2014</p>
EOT;

$content['help'] = <<<EOT
EOT;

$content['premium'] = <<<EOT
<p>It is not always simple to get new players to join your server, but we might be able to help you with that. Our unique Premium system allows server owners like you to increase their server's popularity. You decide how much you pay, and we expose your server accordingly. You can pay any amount between 1 and 50 euros (we need to cap the amount that you can donate to keep our system honest).</p> 
    
    <hr/>
    <h3>Starting at €1/month</h3>
    <p>Starting at €1, your server will get a premium badge in the :serverlist_game serverlist and your own uploaded banner will be displayed instead of default generated one. This will attract the attention of our visitors, hence your server will welcome new players.</p>
    <ul>
      <li>
        Premium badge in serverlist
      </li>
      <li>
        Own uploaded banner instead of generated one
      </li>
    </ul>
    <hr/>
    <h3>Starting at €5/month</h3>
    <p>Starting at €5 your server will also be shown in the Premium :serverlist_game Servers section (which is located above the 'normal' serverlist, and is inserted in the serverlist itself once in a while). Your server will also be marked with a premium background colour in our serverlist, making your :serverlist_game server stand out once more.</p>
    <ul>
      <li>
        Premium server section
        <ul>
          <li>
            The more you pay, the more your server will be shown in the Premium :serverlist_game Servers section.
          </li>
        </ul>
      </li>
      <li>Premium background colour in serverlist</li>
    </ul>
    <hr/>
    <h3>Starting at €20/month</h3>
    <p>Starting at €20 your server will also be included in our banner-rotator (located at the top and bottom of each page on the website). Advertisements from other :serverlist_game servers will also not be shown on your own server page.</p>
    <ul>
      <li>
        Banner rotator
        <ul>
          <li>
            The more you pay, the more your server will be exposed through our banner-rotator.
          </li>
        </ul>
      </li>
      <li>No server advertisements on your server page</li>
    </ul>
    <hr/>
EOT;
return $content;