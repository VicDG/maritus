<?php
class CustomValidator extends Illuminate\Validation\Validator {
  public function validatecheckStatus($attribute, $value, $parameters) {
    if(!empty($parameters) && is_array($parameters)) {
      $type = $parameters[0];
      $refresh = new Refresh($this->getValue('ip'), (($type == 'ping') ? $value : $this->getValue('port')), (($type == 'query') ? $value : null));
      return $refresh->$type();
    }
  }
}