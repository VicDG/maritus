<?php

/*
|--------------------------------------------------------------------------
| Register The Laravel Class Loader
|--------------------------------------------------------------------------
|
| In addition to using Composer, you may use the Laravel class loader to
| load your controllers and models. This is useful for keeping all of
| your classes in the "global" namespace without Composer updating.
|
*/

ClassLoader::addDirectories(array(

	app_path().'/commands',
	app_path().'/controllers',
	app_path().'/models',
	app_path().'/database/seeds',

));

define('SERVERLIST_NAME', 'Minequery');
define('SERVERLIST_GAME', 'Minecraft');

/*
|--------------------------------------------------------------------------
| Application Error Logger
|--------------------------------------------------------------------------
|
| Here we will configure the error logger setup for the application which
| is built on top of the wonderful Monolog library. By default we will
| build a basic log file setup which creates a single file for logs.
|
*/

Log::useFiles(storage_path().'/logs/laravel.log');

/*
|--------------------------------------------------------------------------
| Application Error Handler
|--------------------------------------------------------------------------Â
|
| Here you may handle any errors that occur in your application, including
| logging them or displaying custom views for specific errors. You may
| even register several error handlers to handle different types of
| exceptions. If nothing is returned, the default error view is
| shown, which includes a detailed stack trace during debug.
|
*/
use Illuminate\Database\Eloquent\ModelNotFoundException;
App::error(function(ModelNotFoundException $exception, $code)
{
	App::abort('404');
});

App::fatal(function($exception)
{
	if($exception->getMessage()) {
	  $message = $exception->getMessage();
	}
	else {
	  $message = 'Fatal Error';
	}
	Log::error($exception);
	return Response::view('errors.global', array('message' => $message, 'code' => 500), 500);
});

App::missing(function($exception)
{
	if($exception->getMessage()) {
	  $message = $exception->getMessage();
	}
	else {
	  $message = 'Not found';
	}
    return Response::view('errors.global', array('message' => $message, 'code' => 404), 404);
});

App::error(function(Exception $exception, $code){
	if($exception->getMessage()) {
	  $message = $exception->getMessage();
	}
	else {
      switch($code) {
        case 400:
          $message = 'Bad Request';
          break;
        case 401:
          $message = 'Unauthorized';
          break;
        case 403:
          $message = 'Forbidden';
          break;
	    case 404:
	      $message = 'Not Found';
	      break;
	    case 500:
	      $message = 'Runtime Error';
	      break;
	    default:
	      $message = 'Runtime Error';
	      break;   
      }
	}
	return Response::view('errors.global', array('message' => $message, 'code' => $code), $code); 

});

/*
|--------------------------------------------------------------------------
| Maintenance Mode Handler
|--------------------------------------------------------------------------
|
| The "down" Artisan command gives you the ability to put an application
| into maintenance mode. Here, you will define what is displayed back
| to the user if maintenance mode is in effect for the application.
|
*/

App::down(function()
{
	return Response::make("Be right back!", 503);
});

/*
|--------------------------------------------------------------------------
| Require The Filters File
|--------------------------------------------------------------------------
|
| Next we will load the filters file for the application. This gives us
| a nice separate location to store our route and application filter
| definitions instead of putting them all in the main routes file.
|
*/
require app_path().'/filters.php';
require app_path().'/validations.php';

ClassLoader::addDirectories(array(
    app_path().'/lib',
));

ClassLoader::register(new ClassLoader(array(
    app_path().'/lib',
)));

Validator::resolver(function($translator, $data, $rules, $messages)
{
  return new CustomValidator($translator, $data, $rules, $messages);
});