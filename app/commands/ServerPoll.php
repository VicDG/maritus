<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ServerPoll extends Command
{
    
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'server:poll';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refreshes a server.';
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        set_time_limit(10);
        $id     = $this->argument('id');
        $server = Server::findOrFail($id);
        $return = $server->refresh();
    }
    
    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array(
                'id',
                InputArgument::REQUIRED,
                'The server ID'
            )
        );
    }
    
}
