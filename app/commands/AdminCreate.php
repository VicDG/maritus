<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;

class AdminCreate extends Command
{
    
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'admin:create';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates an Admin account';
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $name            = $this->argument('name');
        $password        = $this->argument('password');
        $admin           = new Admin;
        $admin->name     = $name;
        $admin->password = $password;
        $admin->save();
        $this->line('Created Admin account with name: ' . $admin->name);
    }
    
    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array(
                'name',
                InputArgument::REQUIRED,
                'The Admin Name To be'
            ),
            array(
                'password',
                InputArgument::REQUIRED,
                'Your Requested password'
            )
        );
    }
    
}
