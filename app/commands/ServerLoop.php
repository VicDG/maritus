<?php
require_once(app_path() . '/lib/Parallel/Wrapper.php');
use Carbon\Carbon;
use Illuminate\Console\Command;
use Parallel\Exceptions\InvalidBinaryException;
use Parallel\Wrapper;

class ServerLoop extends Command
{
    
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'server:loop';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Spawns multiple poll processes in parallel.';
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        // Variables
        $parallel = new Wrapper();
        $workers  = 7;
        $lock     = app_path() . '/extras/poll.lock';
        
        // Before kill, remove lockfile
        function sigint()
        {
            unlink($lock);
            exit;
        }
        pcntl_signal(SIGINT, 'sigint');
        pcntl_signal(SIGTERM, 'sigint');
        
        // Start Parallel
        try {
            // Protect script from poll overlapping
            while (file_exists($lock)) {
                sleep(10);
            }
            
            // Touch lockfile
            $fp = fopen($lock, "a+");
            fclose($fp);
            
            // Supply some information to user
            $this->line('Booting up poll cycle: ' . Carbon::now()->toDateTimeString());
            
            // We need to tell the warpper where it can find the binary
            $parallel->initBinary('/usr/bin/parallel');
            
            /*// Add the commands
            foreach (Server::get(array(
                'id'
            )) as $server) {
                $parallel->addCommand('bash -c "/usr/bin/php' . ' ' . base_path() . '/' . 'artisan' . ' ' . 'server:poll' . ' ' . $server->id . '"');
            }*/
            
            for ($i = 1; $i <= 2000; $i++) {
	          $parallel->addCommand('bash -c "/usr/bin/php' . ' ' . base_path() . '/' . 'artisan' . ' ' . 'server:poll' . ' ' . $i . '"');
            }
            // Set amount of 'workers'
            $parallel->setParallelism('auto');
            $parallel->setMaxParallelism($workers);
            
            // Run commands
            $parallel->run();
            
            // Supply more information
            $this->line('Shutting down poll cycle: ' . Carbon::now()->toDateTimeString());
            
            // Remove lockfile
            unlink($lock);
        }
        catch (InvalidBinaryException $e) {
        }
    }
}
