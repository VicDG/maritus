<?php

use Illuminate\Console\Command;

class PremiumRecalc extends Command
{
    
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'premium:recalc';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Recalculates displayIntervals and deletes servers';
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        // Delete old donates
        $expire = Premium::where('created_at', '<', DB::raw('NOW() - INTERVAL 1 MONTH'));
        foreach ($expire->get() as $premium) {
            $server = $premium->server;
            Mail::send('mail/premium/expired', array(
                'name' => $server->name,
                'link' => route('premium')
            ), function($message) use ($server)
            {
                $message->to($server->email)->subject('Premium Expired');
            });
        }
        $expire->delete();
        
        // Recalculate all
        $premium = Premium::where('created_at', '>=', DB::raw('NOW() - INTERVAL 1 MONTH'));
        foreach ($premium->get() as $premium) {
            $premium->setInterval();
            $premium->save();
        }
        Ad::create(array());
        // Recalc sum
        // Cache::forever('premium_total', Premium::where('created_at', '>=', DB::raw('NOW() - INTERVAL 1 MONTH'))->sum('paid'));
    }
    
    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }
    
}
