<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Carbon\Carbon;

class Tasks extends Command
{
    
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'task:do';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Does tasks.';
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $task = $this->argument('task');
        switch ($task) {
            case 'purgeplayers':
                Player::where('created_at', '<', Carbon::now()->subMonth())->delete();
                break;
            case 'votereset':
                Vote::all()->delete();
                
                // Also nuke the offline servers with low uptime
                foreach (DB::select('SELECT servers.id FROM servers JOIN caches ON caches.server_id=servers.id WHERE servers.id NOT IN (SELECT server_id FROM premiums) AND servers.uptime < 10 AND caches.status = 0') as $id) {
					Server::find($id->id)->delete();
                }
                break;
            case 'versions':
                $versions = Array();
                foreach (Server::all() as $server) {
                    array_push($versions, $server->cache->version);
                }
                $versions = array_unique($versions);
                usort($versions, function($a, $b)
                {
                    return -1 * version_compare($a, $b);
                });
                Cache::forever('versions', json_encode($versions));
                break;
            case 'banner':
                $id     = $this->argument('id');
                Banner::cache($id);
                break;
            case 'premium':
                $id            = $this->argument('id');
                $amount        = $this->argument('three');
                $server        = Server::name($id);
                $server->addPremium($amount);
                break;
            case 'newAds':
                Ad::create(array());
                break;
            case 'initPoll':
            	foreach(Server::get(array('id')) as $server) {
	            	if ($server->cache->status) {
		            	Queue::push('PollServer', array('id' => $server->id), 'online');
	            	}
	            	else {
		            	Queue::push('PollServer', array('id' => $server->id), 'offline');
	            	}
            	}
            	break;
            case 'clearBroken':
                foreach (DB::select('SELECT servers.id FROM servers JOIN caches ON servers.id = caches.server_id WHERE caches.success=0 AND caches.fail > 1') as $id) {
					Server::find($id->id)->delete();
                }
            	break;
            case 'oops':
              	foreach(
              	    DB::select("SELECT servers.id FROM servers JOIN caches ON servers.id=caches.server_id WHERE TIMESTAMPDIFF(MINUTE, caches.updated_at, UTC_TIMESTAMP()) > 60") as $id) {
              	    	$server = Server::find($id->id);
			  			if ($server->cache->status) {
		            		Bean::push('PollServer', array('id' => $server->id), 'online', 0);
	            		}
	            		else {
		            		Bean::push('PollServer', array('id' => $server->id), 'offline', 0);
	            		}

			  	    }
			  	break;
			case 'reinforce':
			    $tube = $this->argument('id');
			    $amount = $this->argument('three');
				exec('parallel -n0 "php artisan queue:work --daemon --queue='. $tube .'" ::: {1..'. $amount .'}');
			    break;
            default:
                echo 'You what mate\n';
                break;
        }
    }
    
    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array(
                'task',
                InputArgument::REQUIRED,
                'The task you would like to execute'
            ),
            array(
                'id',
                InputArgument::OPTIONAL,
                'Server Id for task if necessairy'
            ),
            array(
                'three',
                InputArgument::OPTIONAL,
                'Third argument'
            )
        );
    }
    
}
