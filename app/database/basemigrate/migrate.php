<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Server extends Migration
{
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->string('name');
            
            $table->string('password');
            $table->string('salt');
        });
        Schema::create('ads', function($table)
        {
            $table->increments('id');
            $table->bigInteger('displays');
            $table->timestamps();
        });
        Schema::create('caches', function($table)
        {
            $table->increments('id');
            $table->string('motd');
            $table->string('version');
            $table->integer('success');
            $table->integer('fail');
            $table->boolean('status');
            $table->longText('favicon');
            $table->string('server_id');
            $table->timestamps();
        });
        Schema::create('keyword_server', function($table)
        {
            $table->increments('id');
            $table->string('keyword_id');
            $table->string('order');
        });
        Schema::create('keywords', function($table)
        {
            $table->increments('id');
            $table->string('name');
            $table->unique('name');
            $table->integer('server_id');
        });
        Schema::create('players', function($table)
        {
            $table->increments('id');
            $table->string('server_id');
            $table->string('cache_id');
            $table->integer('count');
            $table->integer('max');
            $table->string('names');
            $table->timestamps();
        });
        Schema::create('servers', function($table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            
            $table->string('name');
            $table->index('name');
            $table->string('email');
            
            $table->string('job_id');
            $table->int('job_tries');
            
            $table->string('password');
            $table->string('salt');
            $table->string('reset');
            $table->string('remember_token', 256);
            $table->text('description');
            $table->string('website');
            $table->string('country', 2);
            $table->float('uptime');
            $table->integer('score');
            $table->integer('votes');
            $table->string('banner')->nullable();
            
            $table->string('ip');
            $table->string('port')->default('25565');
            $table->string('query')->nullable();
            $table->string('votifier_ip')->nullable();
            $table->string('votifier_port')->nullable();
            $table->text('votifier_key')->nullable();
            
            // Some cached variables for listing purposes
            $table->integer('playercount');
            $table->integer('playermax');
            $table->string('version');
        });
        Schema::create('premiums', function($table)
        {
            $table->increments('id');
            $table->timestamp('lastDisplay');
            $table->integer('displayInterval');
            $table->index('displayInterval');
            $table->integer('paid');
            $table->index('paid');
            $table->integer('adDisplays');
            $table->string('server_id');
            $table->timestamps();
        });
        Schema::create('votes', function($table)
        {
            $table->increments('id');
            $table->string('player');
            $table->string('ip');
            $table->string('server_id');
            $table->timestamps();
        });
        Schema::create('reviews', function($table)
        {
            $table->increments('id');
            $table->string('server_id');
            $table->string('player');
            $table->text('review');
            $table->timestamps();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
    
}
